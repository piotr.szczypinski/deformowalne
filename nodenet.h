#ifndef NODENET_H
#define NODENET_H

#include <vector>

template< typename TFloat, unsigned int VDimensions, unsigned int VSteps = 1, unsigned int VVectors = 1>
class Node
{
public:
    typedef TFloat FloatType;
    const static int Dimensions = VDimensions;
    const static int Steps = VSteps;
    const static int Vectors = VVectors;
    TFloat referenceCoords[VDimensions];
    TFloat coordinates[VSteps][VDimensions];
    TFloat vectors[VVectors][VDimensions];
    std::vector <unsigned int> neighbors;
};

template< typename TFloat, unsigned int VDimensions, unsigned int VSteps = 1, unsigned int VVectors = 1>
class Network
{
public:
    typedef TFloat FloatType;
    const static int Dimensions = VDimensions;
    const static int Steps = VSteps;
    const static int Vectors = VVectors;

    void addNode(FloatType coordinates[VDimensions], FloatType referenceCoords[VDimensions])
    {
        Node <FloatType, Dimensions, Steps, Vectors> newnode;
        for(unsigned int d = 0; d < Dimensions; d++)
            for(unsigned int t = 0; t < Steps; t++)
            {
                newnode.coordinates[t][d] = coordinates[d];
                newnode.referenceCoords[t][d] = referenceCoords[d];
            }
        for(unsigned int d = 0; d < Dimensions; d++)
            for(unsigned int v = 0; v < Vectors; v++)
                newnode.vectors[v][d] = 0;
        nodes.push_back(newnode);
    }

    unsigned int count(void)
    {
        return nodes.size();
    }

    void addConnection(unsigned int nodea, unsigned int nodeb)
    {
        nodes[nodea].neighbors.push_back(nodeb);
        nodes[nodeb].neighbors.push_back(nodea);
    }

    void removeConnection(unsigned int nodea, unsigned int nodeb)
    {
        unsigned int i;
        unsigned int sn = nodes[nodea].neighbors.size();
        for(i = sn-1; i < sn; i--)
        {
            if(nodes[nodea].neighbors[i] == nodeb)
                nodes[nodea].neighbors.erase(i);
        }
        sn = nodes[nodeb].neighbors.size();
        for(i = sn-1; i < sn; i--)
        {
            if(nodes[nodeb].neighbors[i] == nodea)
                nodes[nodeb].neighbors.erase(i);
        }
    }

    void removeAll(void)
    {
        nodes.clear();
    }

    void getNode(unsigned int node, FloatType coordinates[VDimensions])
    {
        unsigned int t = time % Steps;
        for(unsigned int d = 0; d < Dimensions; d++)
            coordinates[d] = nodes[node].coordinates[t][d];
    }

    void setVector(unsigned int node, unsigned int index, FloatType vector[VDimensions])
    {
        for(unsigned int d = 0; d < Dimensions; d++)
            nodes[node].vectors[index][d] = vector[d];
    }

    unsigned int neighborsCount(unsigned int node)
    {
        if(node >= count()) return 0;
        return nodes[node].neighbors.size();
    }

    unsigned int getNeighbors(unsigned int node, unsigned int index)
    {
        return nodes[node].neighbors[index];
    }

    void updateCoordinates(void)
    {
        time++;
        unsigned int nn = count();
        for(unsigned int n = 0; n < nn; n++)
        {
            for(unsigned int d = 0; d < Dimensions; d++)
            {
                FloatType shift = 0;
                for(unsigned int v = 0; v < Vectors; v++)
                {
                    shift += nodes[n].vectors[v][d];
                }
                FloatType predict = 0;
                for(unsigned int s = 0; s < Steps; s++)
                {
                    predict += (nodes[n].coordinates[(s+time) % Steps][d] * timeWeights[s]);
                }
                nodes[n].coordinates[time % Steps] = predict+shift;
            }
        }
    }

    FloatType timeWeights[Steps];

private:
    std::vector <Node <FloatType, Dimensions, Steps, Vectors> > nodes;
    unsigned int time;
};

#endif // NODENET_H
