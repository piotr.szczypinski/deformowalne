/*
 * Registers images elastically by 3D grid deformation
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "network.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include "../qmazda/MzShared/getoption.h"






#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkVector.h"
#if ITK_VERSION_MAJOR < 4
#include "itkDeformationFieldTransform.h"
#include "itkDeformationFieldSource.h"
#else
#include "itkVectorLinearInterpolateImageFunction.h"
#include "itkDisplacementFieldTransform.h"
#include "itkLandmarkDisplacementFieldSource.h"
#endif
#include "itkResampleImageFilter.h"


const     unsigned int   Dimension = 3;
typedef itk::Image< float, Dimension > ImageType;

char* src_image = NULL;
char* frame = NULL;
char* mask1 = NULL;
char* mask2 = NULL;
char* mask3 = NULL;
char* mask4 = NULL;
char* mask5 = NULL;

bool isprinthelp = false;

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-i", "--src-image", src_image)
        else GET_STRING_OPTION("-f", "--frame", frame)
        else GET_STRING_OPTION("-1", "--mask1", mask1)
        else GET_STRING_OPTION("-2", "--mask2", mask2)
        else GET_STRING_OPTION("-3", "--mask3", mask3)
        else GET_STRING_OPTION("-4", "--mask4", mask4)
        else GET_STRING_OPTION("-5", "--mask5", mask5)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Creates a grid from probabbility image\n");
    printf("2018 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -i, --src-image <file>    Load image from <file>.\n");
    printf("  -f, --frame <str>         Frame identifier for first coloumn.\n");
    printf("  -1, --mask1 <file>        Load floting point mask #1 from <file>.\n");
    printf("  -2, --mask2 <file>        Load floting point mask #2 from <file>.\n");
    printf("  -3, --mask3 <file>        Load floting point mask #3 from <file>.\n");
    printf("  -4, --mask4 <file>        Load floting point mask #4 from <file>.\n");
    printf("  -5, --mask5 <file>        Load floting point mask #5 from <file>.\n");
    printf("  /?, --help                Display this help and exit\n\n");
}



/**
 * @brief metoda3 ładuje policzone wcześniej pole wektorowe i deformuje obraz zgodnie z tym polem
 * @return
 */

double meanmask(ImageType::Pointer image, char* mask, double* n)
{
    double mean = 0;
    double norm = 0;
    ImageType::Pointer maska;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader = ReaderType::New();
    try
    {
        reader->SetFileName(mask);
        reader->Update();
        maska = reader->GetOutput();
    }
    catch(...)
    {
        fprintf(stderr, "Cannot load image %s\n", mask);
        return -4;
    }
    typedef itk::ImageRegionIterator< ImageType > IteratorType;
    IteratorType mIterator(maska, maska->GetRequestedRegion());
    IteratorType iIterator(image, image->GetRequestedRegion());
    mIterator.GoToBegin();
    iIterator.GoToBegin();
    for (; !(mIterator.IsAtEnd() || iIterator.IsAtEnd()); ++iIterator, ++mIterator)
    {
        mean += (iIterator.Get()*mIterator.Get());
        norm += mIterator.Get();
    }
    *n = norm;
    return mean/norm;
}



int main(int argc, char *argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }
    if(src_image == NULL)
    {
        fprintf(stderr, "Unset --src-image\n");
        return -1;
    }
    
    typename ImageType::Pointer image;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typename ReaderType::Pointer reader = ReaderType::New();
    try
    {
        reader->SetFileName(src_image);
        reader->Update();
        image = reader->GetOutput();
    }
    catch(...)
    {
        fprintf(stderr, "Cannot load image %s\n", src_image);
        return -4;
    }

    double m;
    double n;
    if(frame != NULL)
        printf("%s", frame);

    if(mask1 != NULL)
    {
        m = 0.0;
        n = 0.0;
        m = meanmask(image, mask1, &n);
        printf("\t%f\t%f", (float) m, (float)n);
    }
    if(mask2 != NULL)
    {
        m = 0.0;
        n = 0.0;
        m = meanmask(image, mask2, &n);
        printf("\t%f\t%f", (float) m, (float)n);
    }
    if(mask3 != NULL)
    {
        m = 0.0;
        n = 0.0;
        m = meanmask(image, mask3, &n);
        printf("\t%f\t%f", (float) m, (float)n);
    }
    if(mask4 != NULL)
    {
        m = 0.0;
        n = 0.0;
        m = meanmask(image, mask4, &n);
        printf("\t%f\t%f", (float) m, (float)n);
    }
    if(mask5 != NULL)
    {
        m = 0.0;
        n = 0.0;
        m = meanmask(image, mask5, &n);
        printf("\t%f\t%f", (float) m, (float)n);
    }


    printf("\n");
    
    return 0;
}
