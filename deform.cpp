/*
 * Registers images elastically by 3D grid deformation
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "network.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include "../qmazda/MzShared/getoption.h"

char* fixed_image = NULL;
char* moved_image = NULL;
char* input_grid = NULL;
char* output_grid = NULL;
char* tension_mode = NULL;
char* image_mode = NULL;

int block_radius = -1;
int search_radius = -1;

int rigid_iterations = -1;
int flex_iterations = -1;

float rigidity_wieght = std::numeric_limits<float>::quiet_NaN();
float image_wieght = std::numeric_limits<float>::quiet_NaN();
bool isprinthelp = false;

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-f", "--fixed-image", fixed_image)
        else GET_STRING_OPTION("-m", "--moved-image", moved_image)
        else GET_STRING_OPTION("-g", "--input-grid", input_grid)
        else GET_STRING_OPTION("-o", "--outpt-grid", output_grid)
        else GET_STRING_OPTION("-t", "--transform-mode", tension_mode)
        else GET_STRING_OPTION("-u", "--image-mode", image_mode)

        else GET_FLOAT_OPTION("-i", "--rigidity-wieght", rigidity_wieght)
        else GET_FLOAT_OPTION("-e", "--image-wieght", image_wieght)
        else GET_INT_OPTION("-b", "--block-radius", block_radius)
        else GET_INT_OPTION("-s", "--search-radius", search_radius)
        else GET_INT_OPTION("-R", "--rigid-iters", rigid_iterations)
        else GET_INT_OPTION("-F", "--flex-iters", flex_iterations)

        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Registers images elastically by deforming 3D grid\n");
    printf("2018 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -f, --fixed-image <file>    Load fixed image from <file>.\n");
    printf("  -m, --moved-image <file>    Load moved image from <file>.\n");
    printf("  -g, --input-grid <file>     Load input grid from <file>.\n");
    printf("  -o, --outpt-grid <file>     Save resulting grid to <file>.\n");
    printf("  -i, --rigidity-wieght <fp>  Set rigidity (internal forces) weight.\n");
    printf("  -e, --image-wieght <fp>     Set image (external force) weight.\n");
    printf("  -b, --block-radius <int>    Block size for matching.\n");
    printf("  -s, --search-radius <int>   Search radius for matching.\n");
    printf("  -t, --transform-mode <str>  Set one of the local transformation modes:\n");
    printf("      AFFIN - Affine: translation, resizing, rotation, shear,\n");
    printf("      PROCR - Procrustes: translation, resizing, rotation,\n");
    printf("      UNIMO - Unimodal: translation, rotation, shear,\n");
    printf("      ORTHO - Orthonormal: translation, rotation,\n");
    printf("      ROTAT - Quaternion: translation, rotation,\n");
    printf("      ROTSZ - Quaternion: translation, resizing, rotation.\n");
    printf("  -u, --image-mode <str>  Set one of the image modes:\n");
    printf("      MAXMAD, GRADMAD, MAXHBM, GRADHBM\n");
    printf("  -R, --rigid-iters <int>     Set number of evolutions for rigid registration,\n");
    printf("  -F, --flex-iters <int>      Set number of evolutions for non-rigid registration\n");
    printf("  /?, --help                  Display this help and exit\n\n");
}

int main(int argc, char *argv[])
{
    int i;
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }

    if(tension_mode == NULL)
    {
        fprintf(stderr, "Unset --tension-mode\n");
        return -1;
    }
    if(fixed_image == NULL)
    {
        fprintf(stderr, "Unset --fixed-image\n");
        return -1;
    }
    if(moved_image == NULL)
    {
        fprintf(stderr, "Unset --moved-image\n");
        return -1;
    }
    if(input_grid == NULL)
    {
        fprintf(stderr, "Unset --input-grid\n");
        return -1;
    }
    if(output_grid == NULL)
    {
        fprintf(stderr, "Unset --output-grid\n");
        return -1;
    }
    if(block_radius <= 0)
    {
        fprintf(stderr, "Unset --block-radius\n");
        return -1;
    }
    if(search_radius <= 0)
    {
        fprintf(stderr, "Unset --search-radius\n");
        return -1;
    }
    if(!(rigidity_wieght == rigidity_wieght))
    {
        fprintf(stderr, "Unset --rigidity-wieght\n");
        return -1;
    }
    if(!(image_wieght == image_wieght))
    {
        fprintf(stderr, "Unset --image-wieght\n");
        return -1;
    }
    if(rigid_iterations < 0)
    {
        fprintf(stderr, "Unset --rigid-iters\n");
        return -1;
    }
    if(flex_iterations < 0)
    {
        fprintf(stderr, "Unset --flex-iters\n");
        return -1;
    }


    TensionsMode tmode;
    if(strcmp("AFFIN", tension_mode) == 0) tmode = MODE_AFFIN;
    else if(strcmp("PROCR", tension_mode) == 0) tmode = MODE_PROCR;
    else if(strcmp("UNIMO", tension_mode) == 0) tmode = MODE_UNIMO;
    else if(strcmp("ORTHO", tension_mode) == 0) tmode = MODE_ORTHO;
    else if(strcmp("ROTAT", tension_mode) == 0) tmode = MODE_ROTAT;
    else if(strcmp("ROTSZ", tension_mode) == 0) tmode = MODE_ROTSZ;
    else
    {
        fprintf(stderr, "Unrecognized transformation mode.\n");
        return -1;
    }

    ImageMode imode;
    if(strcmp("MAXMAD", image_mode) == 0) imode = MODE_MAXMAD;
    else if(strcmp("MAXHBM", image_mode) == 0) imode = MODE_MAXHBM;
    else if(strcmp("GRADMAD", image_mode) == 0) imode = MODE_GRADMAD;
    else if(strcmp("GRADHBM", image_mode) == 0) imode = MODE_GRADHBM;
    else
    {
        fprintf(stderr, "Unrecognized transformation mode.\n");
        return -1;
    }


    ExternalType* ext = new ExternalType(imode);
    ext->setRadi(block_radius, search_radius);
    if(! ext->loadFixed(fixed_image))
    {
        fprintf(stderr, "Cannot load image %s\n", fixed_image);
        return -2;
    }
    if(! ext->loadMoved(moved_image))
    {
        fprintf(stderr, "Cannot load image %s\n", moved_image);
        return -2;
    }

    double params[2];
    params[0] = rigidity_wieght;
    params[1] = image_wieght;

    Network network(params, tmode, ext);
    if(! network.load(input_grid))
    {
        fprintf(stderr, "Cannot load grid %s\n", input_grid);
        return -2;
    }


    // network.save("./aaa.txt");

    for(i = 0; i < rigid_iterations; i++)
    {
        network.iterateRigid();
        printf("+");
        fflush(stdout);
    }

    for(i = 0; i < flex_iterations; i++)
    {
        network.iterateFlexible();
        printf("x");
        fflush(stdout);
    }

    if(! network.save(output_grid))
    {
        fprintf(stderr, "Cannot save grid %s\n", output_grid);
        return -3;
    }
    return 0;
}
