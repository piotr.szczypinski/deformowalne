TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../deformowalne/Executables
TARGET = interpol
TEMPLATE = app

SOURCES += interpol.cpp\
        affinefit.cpp \
    quatern.cpp \
    node.cpp \
    network.cpp \
    internal.cpp

HEADERS  += affinefit.h \
    quatern.h \
    node.h \
    network.h \
    external.h \
    internal.h

#LIBS += -lalglib

include(../qmazda/Pri/config.pri)
include(../qmazda/Pri/alglib.pri)
include(../qmazda/Pri/itk.pri)
