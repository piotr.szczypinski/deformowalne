/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AXISANGL_H
#define AXISANGL_H


bool matrixFromAxisAngle(double* matrix, const double* axis, const double* complex);

bool averageDirectionPca(double* result, const double* input, const unsigned int dim, const unsigned int count);

double dotProduct(const double *wector, const double *vector);

void crossProduct(double* axis, const double *wector, const double *vector);

void getDirectionsFromMean(double *directions, const double *vectors, const unsigned int count);

void getAxesFromVectors(double *axes, const double *vectors1, const double *vectors2, const unsigned int count);

void getRotationsAsComplex(double* complex, double* axes, double* origin, double* current, const unsigned int count);

bool normalizedAverageVectors(double* result, const double* input, const unsigned int dim, const unsigned int count);

bool fitOrthogonalAxisAngle(double *matrix, double* buffer, double *qorigin, double *qcurrent, const double *origin, const double *current, const unsigned int count);

#endif // AXISANGL_H
