/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Refs:
// https://www.gamasutra.com/view/feature/131686/rotating_objects_using_quaternions.php
// https://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
// Markley, F. L., Cheng, Y., Crassidis, J. L., & Oshman, Y. (2007). Averaging quaternions. Journal of Guidance Control and Dynamics, 30(4), 1193.
//     http://www.acsu.buffalo.edu/~johnc/ave_quat07.pdf
// http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors (for comparison, not implemented here)

#include "quatern.h"
#include <math.h>

// Makes use of alglib library to compute inverse matrix, singular values
#include <ap.h>
#include <linalg.h>
#include "affinefit.h"

const double e_zero = 0.00001;
const double e_one = 0.9998;

void quaternionFromAxisAndAngle(double* quat, const double* axis, const double angle)
{
    double sina = sin(angle/2);
    quat[0] = cos(angle/2);
    quat[1] = axis[0] * sina;
    quat[2] = axis[1] * sina;
    quat[3] = axis[2] * sina;
}

void quaternionFromVectors(double* quat, const double *wector, const double *vector)
{
    double axis[3];
    double dot, angle;
    double wnorm, vnorm, anorm, dnorm;
//Cross product:
    axis[0] = wector[1]*vector[2] - wector[2]*vector[1];
    axis[1] = wector[2]*vector[0] - wector[0]*vector[2];
    axis[2] = wector[0]*vector[1] - wector[1]*vector[0];
//Dot product
    dot = wector[0]*vector[0] + wector[1]*vector[1] + wector[2]*vector[2];
//Norms
    anorm = sqrt(axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2]);
    wnorm = (wector[0]*wector[0]+wector[1]*wector[1]+wector[2]*wector[2]);
    vnorm = (vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2]);
    dnorm = sqrt(wnorm*vnorm);
    dot /= dnorm;

    if(dot > e_one)
    {
        quat[0] = 1.0;
        quat[1] = 0.0;
        quat[2] = 0.0;
        quat[3] = 0.0;
    }
    else if(dot < -e_one)
    {
        quat[0] = 0.0; //Problematic
        quat[1] = 0.0;
        quat[2] = 0.0;
        quat[3] = 0.0;
    }
    else
    {
        axis[0] /= anorm;
        axis[1] /= anorm;
        axis[2] /= anorm;
        angle = acos(dot);
//Quaternion
        quaternionFromAxisAndAngle(quat, axis, angle);
//        dnorm = sqrt(dnorm);
        quat[0] *= dnorm;
        quat[1] *= dnorm;
        quat[2] *= dnorm;
        quat[3] *= dnorm;
    }
}

void quaternionToRotationMatrix(double* mat, const double* quat)
{
    double wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;
    x2 = quat[1] + quat[1];
    y2 = quat[2] + quat[2];
    z2 = quat[3] + quat[3];
    xx = quat[1] * x2;
    xy = quat[1] * y2;
    xz = quat[1] * z2;
    yy = quat[2] * y2;
    yz = quat[2] * z2;
    zz = quat[3] * z2;
    wx = quat[0] * x2;
    wy = quat[0] * y2;
    wz = quat[0] * z2;

    mat[0] = 1.0 - (yy + zz);
    mat[1] = xy - wz;
    mat[2] = xz + wy;
//    mat[3] = 0.0;

    mat[4] = xy + wz;
    mat[5] = 1.0 - (xx + zz);
    mat[6] = yz - wx;
//    mat[7] = 0.0;

    mat[8] = xz - wy;
    mat[9] = yz + wx;
    mat[10] = 1.0 - (xx + yy);
//    mat[11] = 0.0;

//    mat[12] = 0;
//    mat[13] = 0;
//    mat[14] = 0;
//    mat[15] = 1;
}

bool quaternionPcaAverage(double* output, const double* input, const unsigned int dim, const unsigned int count)
{
    alglib::real_2d_array covar;
    alglib::real_1d_array evalus;
    alglib::real_2d_array evctrs;

    covar.setlength(dim, dim);
    evalus.setlength(dim);
    evctrs.setlength(dim, dim);

// Matrix computation
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) = 0.0;
        }
    }
    for(unsigned int v = 0; v < count; v++)
    {
        for(unsigned int f1 = 0; f1 < dim; f1++)
        {
            double ff1 = input[dim*v + f1];
            for(unsigned int f0 = f1; f0 < dim; f0++)
            {
                double ff0 = input[dim*v + f0];
                covar(f1, f0) += (ff0 * ff1);
            }
        }
    }
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) /= count;
        }
    }
// Eigendecomposition
    if(! alglib::smatrixevd(covar, dim, 1, true, evalus, evctrs))
    {
        return false;
    }
    for(unsigned int j = 0; j < dim; j++)
    {
        output[j] = evctrs(j, dim-1);
    }
    return true;
}

void quaternionMultiplication(double *res, const double* q1, const double* q2)
{
    double A, B, C, D, E, F, G, H;
    A = (q1[0] + q1[1])*(q2[0] + q2[1]);
    B = (q1[3] - q1[2])*(q2[2] - q2[3]);
    C = (q1[0] - q1[1])*(q2[2] + q2[3]);
    D = (q1[2] + q1[3])*(q2[0] - q2[1]);
    E = (q1[1] + q1[3])*(q2[1] + q2[2]);
    F = (q1[1] - q1[3])*(q2[1] - q2[2]);
    G = (q1[0] + q1[2])*(q2[0] - q2[3]);
    H = (q1[0] - q1[2])*(q2[0] + q2[3]);
    res[0] = B + (-E - F + G + H)/2;
    res[1] = A - (E + F + G + H)/2;
    res[2] = C + (E - F + G - H)/2;
    res[3] = D + (E - F - G + H)/2;
}

bool quaternionFitOrthogonalFromDirections(double* quaternion, double *quaternions, const double *origin, const double *current, const unsigned int count)
{
    for(unsigned int s = 0; s < count; s++)
    {
        quaternionFromVectors(quaternions+s*4, origin+3*s, current+3*s);
    }
    if(! quaternionPcaAverage(quaternion, quaternions, 4, count))
    {
        return false;
    }
    return true;
}

void quaternionGetDirections(double *directions, double* mean, const double *vectors, const unsigned int dim, const unsigned int count)
{
    double* pd;
    double* pc = (double*) vectors;

    for(unsigned int k = 0; k < dim; k++)
        mean[k] = 0.0;

    for(unsigned int s = 0; s < count; s++)
    {
        for(unsigned int k = 0; k < dim; k++)
        {
            mean[k] += *pc;
            pc++;
        }
    }
    for(unsigned int k = 0; k < dim; k++)
    {
        mean[k] /= count;
    }

    pc = (double*) vectors;
    pd = directions;
    for(unsigned int s = 0; s < count; s++)
    {
        for(unsigned int k = 0; k < dim; k++)
        {
            *pd = *pc - mean[k]; pc++; pd++;
        }
    }
}

bool quaternionFitOrthogonal(double *matrix, double *quaternion, double *quaternions, const double *qorigin, const double *qcurrent, const unsigned int count)
{
    if(! quaternionFitOrthogonalFromDirections(quaternion, quaternions, qorigin, qcurrent, count))
        return false;
    quaternionToRotationMatrix(matrix, quaternion);
    return true;
}

bool quaternionFitOrthogonalIterative(double *matrix, double *quaternion, double *quaternions, double *qorigin, double *qcurrent, const double *origin, const double *current, const unsigned int count, const unsigned int iterations)
{
    double wmean[3];
    double vmean[3];
    double m1[16];
    double m2[16];
    quaternionGetDirections(qcurrent, vmean, current, 3, count);
    quaternionGetDirections(qorigin, wmean, origin, 3, count);

    if(! quaternionFitOrthogonalFromDirections(quaternion, quaternions, qorigin, qcurrent, count))
        return false;
    quaternionToRotationMatrix(m2, quaternion);

    for(int i1 = 0; i1 < 3; i1++)
        for(int i2 = 0; i2 < 3; i2++)
            matrix[i1*4+i2] = m2[i1*4+i2];

    for(unsigned int i = 0; i < iterations; i++)
    {
        for(unsigned int s = 0; s < count; s++)
        {
            double newref[4];
            for(unsigned int k = 0; k < 3; k++)
            {
                double v = 0.0;
                for(unsigned int d = 0; d < 3; d++)
                {
                    v += qorigin[s*3 + d] * m2[k*(3+1) + d];
                }
                newref[k] = v;
            }
            for(unsigned int k = 0; k < 3; k++)
            {
                qorigin[s*3 + k] = newref[k];
            }
        }
        if(! quaternionFitOrthogonalFromDirections(quaternion, quaternions, qorigin, qcurrent, count))
            return false;
        quaternionToRotationMatrix(m2, quaternion);

        for(int i1 = 0; i1 < 3; i1++)
            for(int i2 = 0; i2 < 3; i2++)
                m1[i1*4+i2] = matrix[i1*4+i2];
        for(int i1 = 0; i1 < 3; i1++)
            for(int i2 = 0; i2 < 3; i2++)
                matrix[i1*4+i2] = m2[i1*4]*m1[i2]+ m2[i1*4+1]*m1[i2+4]+ m2[i1*4+2]*m1[i2+8];
    }
    affineRecomputeTranslation(matrix, wmean, vmean, 3);
    return true;
}

bool complexFitOrthogonalFromDirections(double* average, const double *origin, const double *current, const unsigned int count)
{
    double ar = 0.0;
    double ai = 0.0;

    for(unsigned int s = 0; s < count; s++)
    {
        double r = origin[2*s];
        double i = origin[2*s+1];
        double rr = current[2*s];
        double ii = current[2*s+1];
        ar += (r*rr+i*ii);
        ai += (ii*r-i*rr);
    }
    double n = sqrt(ar*ar+ai*ai);
    if(n < e_zero)
    {
        average[0] = 0.0;
        average[1] = 0.0;
        return false;
    }
    average[0] = ar / n;
    average[1] = ai / n;
    return true;
}

void complexToRotationMatrix(double* mat, const double* cplx)
{
    mat[0] = cplx[0];
    mat[1] = -cplx[1];
    mat[3] = cplx[1];
    mat[4] = cplx[0];
}

bool complexFitOrthogonal(double *matrix, double *complex, const double *qorigin, const double *qcurrent, const unsigned int count)
{
    if(! complexFitOrthogonalFromDirections(complex, qorigin, qcurrent, count))
        return false;
    complexToRotationMatrix(matrix, complex);
    return true;
}

bool complexFitOrthogonal(double *matrix, double *complex, double *qorigin, double *qcurrent, const double *origin, const double *current, const unsigned int count)
{
    double wmean[2];
    double vmean[2];
    quaternionGetDirections(qcurrent, vmean, current, 2, count);
    quaternionGetDirections(qorigin, wmean, origin, 2, count);
    if(! complexFitOrthogonalFromDirections(complex, qorigin, qcurrent, count))
        return false;
    complexToRotationMatrix(matrix, complex);
    affineRecomputeTranslation(matrix, wmean, vmean, 2);
    return true;
}
