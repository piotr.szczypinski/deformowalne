#ifndef NETWORK_H
#define NETWORK_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include "node.h"
#include "external.h"
#include "internal.h"


typedef   unsigned short  PixelType;
typedef External<PixelType, 3> ExternalType;

struct NetworkNode
{
    double c[4];
    unsigned int connections;
    float radius;
};
struct NetworkConnection
{
    unsigned int c[2];
};

class Network
{
public:
    Network(double parameters[], TensionsMode tmode, ExternalType* externObject);
    ~Network();
    bool load(std::ifstream* file);
    bool load(const char *fileName);
    bool save(std::ofstream* file, bool reference = false);
    bool save(const char *fileName, bool reference = false);
    void generateRandom(float subtract, float divide, float distance);
    //void deform(void);
    void iterateRigid(void);
    void iterateFlexible(void);


    void generateRandom(void);
    void generateRandom(double boundingBox[], double density, double linkDistance);
    void deform(void);

    double getNodeCoordinate(unsigned int i, unsigned int d);
    unsigned int getNodesCount(void);


private:
    bool createNetwork(void);

    double parameters[2];
    TensionsMode tmode;

    unsigned int dimensions;
    std::vector <NetworkNode> nodes;
    std::vector <NetworkConnection> connections;
    Node** netnodes;

    ExternalType* extObject;
    Internal* intObject;

    void* intBuffer;
};

#endif // NETWORK_H
