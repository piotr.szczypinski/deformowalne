/*
 * Registers images elastically by 3D grid deformation
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "network.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include "../qmazda/MzShared/getoption.h"






#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkVector.h"
#if ITK_VERSION_MAJOR < 4
#include "itkDeformationFieldTransform.h"
#include "itkDeformationFieldSource.h"
#else
#include "itkVectorLinearInterpolateImageFunction.h"
#include "itkDisplacementFieldTransform.h"
#include "itkLandmarkDisplacementFieldSource.h"
#endif
#include "itkResampleImageFilter.h"


const     unsigned int   Dimension = 3;
//typedef   itk::Image< PixelType, Dimension > ImageType;

char* befor_image = NULL;
char* after_image = NULL;
char* befor_grid = NULL;
char* after_grid = NULL;
char* pixel_type = NULL;
char* deform_field = NULL;

float multiply_intensity = 1.0;

bool isprinthelp = false;

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-i", "--src-image", befor_image)
                else GET_STRING_OPTION("-s", "--src-grid", befor_grid)
                else GET_STRING_OPTION("-d", "--dst-grid", after_grid)
                else GET_STRING_OPTION("-o", "--out-image", after_image)
                else GET_STRING_OPTION("-f", "--deform-field", deform_field)
                else GET_STRING_OPTION("-p", "--pixel-type", pixel_type)
                else GET_FLOAT_OPTION("-m", "--multiply", multiply_intensity)
                else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
                else return argi;
        argi++;
    }
    return 0;
}

void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Creates a grid from probabbility image\n");
    printf("2018 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -i, --src-image <file>    Load image from <file>.\n");
    printf("  -s, --src-grid <file>     Load source grid from <file>.\n");
    printf("  -d, --dst-grid <file>     Load destination grid from <file>\n");
    printf("  -o, --out-image <file>    Save resulting image to <file>\n");
    printf("  -f, --deform-field <file> Deformation field <file>\n");
    printf("  -p  --pixel-type <str>    Pixel type, <str> = u8, u16, s16, f32\n");
    printf("  -m, --multiply <float>    Input image intensity multiplier\n");
    printf("  /?, --help                Display this help and exit\n\n");
}

/**
 * @brief metoda1 na podstawie dwóch siatek, referencyjnej i zdeformowanej, oblicza pole wektorowe i deformuje obraz zgodnie z tym polem
 * @return
 */

template<typename PixelType>
int metoda1(unsigned int anc, Network* before_network, Network* after_network)
{
    typedef itk::Image< PixelType, Dimension > ImageType;

#if ITK_VERSION_MAJOR < 4
    typedef   float           VectorComponentType;
#else
    typedef   double          VectorComponentType;
#endif
    typedef   itk::Vector< VectorComponentType, Dimension >    VectorType;
    typedef   itk::Image< VectorType,  Dimension >   DeformationFieldType;
    typename ImageType::Pointer image;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typename ReaderType::Pointer reader = ReaderType::New();
    try
    {
        reader->SetFileName(befor_image);
        reader->Update();
        image = reader->GetOutput();
    }
    catch(...)
    {
        fprintf(stderr, "Cannot load image %s\n", befor_image);
        return -4;
    }

    if(multiply_intensity != 1.0)
    {
        typedef typename itk::ImageRegionIterator< ImageType > IteratorType;
        IteratorType inputIterator(image, image->GetRequestedRegion());
        for (inputIterator.GoToBegin(); !inputIterator.IsAtEnd(); ++inputIterator)
            inputIterator.Set(inputIterator.Get()*multiply_intensity);
    }

    typename ImageType::PointType origino = image->GetOrigin();
    typename ImageType::PointType origin;
    origin.Fill(0.0);
    image->SetOrigin( origin );
    typename ImageType::DirectionType directiono = image->GetDirection();
    typename ImageType::DirectionType direction;
    direction.SetIdentity();
    image->SetDirection( direction );

#if ITK_VERSION_MAJOR < 4
    typedef itk::DeformationFieldSource<DeformationFieldType>  DeformationFieldSourceType;
#else
    typedef itk::LandmarkDisplacementFieldSource<DeformationFieldType>  DeformationFieldSourceType;
#endif
    DeformationFieldSourceType::Pointer deformationFieldSource = DeformationFieldSourceType::New();
    deformationFieldSource->SetOutputSpacing( image->GetSpacing() );
    deformationFieldSource->SetOutputOrigin(  image->GetOrigin() );
    deformationFieldSource->SetOutputRegion(  image->GetLargestPossibleRegion() );
    deformationFieldSource->SetOutputDirection( image->GetDirection() );

    //  Create source and target landmarks.
    typedef DeformationFieldSourceType::LandmarkContainer          LandmarkContainerType;
    typedef DeformationFieldSourceType::LandmarkPointType          LandmarkPointType;

    LandmarkContainerType::Pointer sourceLandmarks = LandmarkContainerType::New();
    LandmarkContainerType::Pointer targetLandmarks = LandmarkContainerType::New();

    LandmarkPointType sourcePoint;
    LandmarkPointType targetPoint;

    for(unsigned int i = 0; i < anc; i++)
    {
        for(unsigned int d = 0; d < Dimension; d++)
        {
            sourcePoint[d] = before_network->getNodeCoordinate(i, d);
            targetPoint[d] = after_network->getNodeCoordinate(i, d);
        }
        sourceLandmarks->InsertElement( i, sourcePoint );
        targetLandmarks->InsertElement( i, targetPoint );
    }

    deformationFieldSource->SetSourceLandmarks( sourceLandmarks.GetPointer() );
    deformationFieldSource->SetTargetLandmarks( targetLandmarks.GetPointer() );
    deformationFieldSource->UpdateLargestPossibleRegion();

    if(deform_field != NULL)
    {
        typedef itk::ImageFileWriter<  DeformationFieldType  > WriterType;
        WriterType::Pointer writer = WriterType::New();
        writer->SetInput (  deformationFieldSource->GetOutput() );
        writer->SetFileName( deform_field );
        writer->Update();
    }

#if ITK_VERSION_MAJOR < 4
    typedef itk::DeformationFieldTransform<VectorComponentType, Dimension>  DeformationFieldTransformType;
#else
    typedef itk::DisplacementFieldTransform<VectorComponentType, Dimension>  DeformationFieldTransformType;
#endif
    DeformationFieldTransformType::Pointer deformationFieldTransform = DeformationFieldTransformType::New();

#if ITK_VERSION_MAJOR < 4
    deformationFieldTransform->SetDeformationField( deformationFieldSource->GetOutput() );
#else
    deformationFieldTransform->SetDisplacementField( deformationFieldSource->GetOutput() );
#endif
    typedef itk::ResampleImageFilter<ImageType, ImageType, VectorComponentType >    ResampleFilterType;
    typename ResampleFilterType::Pointer resampleFilter = ResampleFilterType::New();
    resampleFilter->SetInput( image );
    resampleFilter->SetTransform( deformationFieldTransform );
    resampleFilter->SetSize( image->GetLargestPossibleRegion().GetSize() );
    resampleFilter->SetOutputOrigin(  image->GetOrigin() );
    resampleFilter->SetOutputSpacing( image->GetSpacing() );
    resampleFilter->SetOutputDirection( image->GetDirection() );
    resampleFilter->SetDefaultPixelValue( 0 );
    resampleFilter->Update();
    typename ImageType::Pointer outimage = resampleFilter->GetOutput();

    outimage->SetOrigin( origino );
    outimage->SetDirection( directiono );

    typedef itk::ImageFileWriter<  ImageType  > WriterType;
    typename WriterType::Pointer writer = WriterType::New();
    writer->SetInput ( outimage );
    writer->SetFileName(after_image);
    writer->Update();

    return 0;
}

/**
 * @brief metoda2 na podstawie dwóch siatek, referencyjnej i zdeformowanej, oblicza pole wektorowe i deformuje obraz zgodnie z tym polem
 * @return
 */
/*
int metoda2(unsigned int anc, Network* before_network, Network* after_network)
{
    ImageType::Pointer image;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    ReaderType::Pointer reader = ReaderType::New();
    try
    {
        reader->SetFileName(befor_image);
        reader->Update();
        image = reader->GetOutput();
    }
    catch(...)
    {
        fprintf(stderr, "Cannot load image %s\n", befor_image);
        return -4;
    }

    if(multiply_intensity != 1.0)
    {
        typedef typename itk::ImageRegionIterator< ImageType > IteratorType;
        IteratorType inputIterator(image, image->GetRequestedRegion());
        for (inputIterator.GoToBegin(); !inputIterator.IsAtEnd(); ++inputIterator)
            inputIterator.Set(inputIterator.Get()*multiply_intensity);
    }

    typedef itk::ThinPlateSplineKernelTransform< double, Dimension > TransformType;
    TransformType::PointSetType::Pointer sourceLandMarks = TransformType::PointSetType::New();
    TransformType::PointSetType::Pointer targetLandMarks = TransformType::PointSetType::New();
    TransformType::PointSetType::PointsContainer::Pointer sourceLandMarkContainer = sourceLandMarks->GetPoints();
    TransformType::PointSetType::PointsContainer::Pointer targetLandMarkContainer = targetLandMarks->GetPoints();
    itk::Point< double, Dimension > sourcePoint;
    itk::Point< double, Dimension > targetPoint;
    for(unsigned int i = 0; i < anc; i++)
    {
        for(unsigned int d = 0; d < Dimension; d++)
        {
            sourcePoint[d] = before_network->getNodeCoordinate(i, d);
            targetPoint[d] = after_network->getNodeCoordinate(i, d);
        }
        sourceLandMarkContainer->InsertElement( i, sourcePoint );
        targetLandMarkContainer->InsertElement( i, targetPoint );
    }
    TransformType::Pointer transformationField = TransformType::New();
    transformationField->SetSourceLandmarks(sourceLandMarks);
    transformationField->SetTargetLandmarks(targetLandMarks);
    transformationField->ComputeWMatrix();

    typedef itk::ResampleImageFilter< ImageType, ImageType > ResamplerType;
    typedef itk::LinearInterpolateImageFunction< ImageType, double > InterpolatorType;
    ResamplerType::Pointer resampler = ResamplerType::New();
    InterpolatorType::Pointer interpolator = InterpolatorType::New();
    resampler->SetInterpolator( interpolator );
    ImageType::SpacingType spacing = image->GetSpacing();
    ImageType::PointType   origin  = image->GetOrigin();
    ImageType::DirectionType direction  = image->GetDirection();
    ImageType::RegionType region = image->GetBufferedRegion();
    ImageType::SizeType   size =  region.GetSize();
    resampler->SetOutputSpacing( spacing );
    resampler->SetOutputDirection( direction );
    resampler->SetOutputOrigin(  origin  );
    resampler->SetSize( size );
    resampler->SetTransform( transformationField );
    resampler->SetOutputStartIndex(  region.GetIndex() );
    resampler->SetInput( image );

    typedef itk::ImageFileWriter<  ImageType  > WriterType;
    WriterType::Pointer writer = WriterType::New();
    writer->SetInput ( resampler->GetOutput() );
    writer->SetFileName(after_image);
    writer->Update();

    return 0;
}
*/


/**
 * @brief metoda3 ładuje policzone wcześniej pole wektorowe i deformuje obraz zgodnie z tym polem
 * @return
 */

template<typename PixelType>
int metoda3(void)
{
    typedef itk::Image< PixelType, Dimension > ImageType;
#if ITK_VERSION_MAJOR < 4
    typedef   float           VectorComponentType;
#else
    typedef   double          VectorComponentType;
#endif
    typedef   itk::Vector< VectorComponentType, Dimension >    VectorType;
    typedef   itk::Image< VectorType,  Dimension >   DeformationFieldType;
    typename ImageType::Pointer image;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typename ReaderType::Pointer reader = ReaderType::New();
    try
    {
        reader->SetFileName(befor_image);
        reader->Update();
        image = reader->GetOutput();
    }
    catch(...)
    {
        fprintf(stderr, "Cannot load image %s\n", befor_image);
        return -4;
    }
    if(multiply_intensity != 1.0)
    {
        typedef typename itk::ImageRegionIterator< ImageType > IteratorType;
        IteratorType inputIterator(image, image->GetRequestedRegion());
        for (inputIterator.GoToBegin(); !inputIterator.IsAtEnd(); ++inputIterator)
            inputIterator.Set(inputIterator.Get()*multiply_intensity);
    }

    typename ImageType::PointType origino = image->GetOrigin();
    typename ImageType::PointType origin;
    origin.Fill(0.0);
    image->SetOrigin( origin );
    typename ImageType::DirectionType directiono = image->GetDirection();
    typename ImageType::DirectionType direction;
    direction.SetIdentity();
    image->SetDirection( direction );

    typedef itk::ImageFileReader< DeformationFieldType > FieldReaderType;
    typename FieldReaderType::Pointer fieldreader = FieldReaderType::New();
    fieldreader->SetFileName( deform_field );
    fieldreader->Update();

#if ITK_VERSION_MAJOR < 4
    typedef itk::DeformationFieldTransform<VectorComponentType, Dimension>  DeformationFieldTransformType;
    DeformationFieldTransformType::Pointer deformationFieldTransform = DeformationFieldTransformType::New();
    deformationFieldTransform->SetDeformationField( fieldreader->GetOutput() );
#else
    typedef itk::DisplacementFieldTransform<VectorComponentType, Dimension>  DeformationFieldTransformType;
    DeformationFieldTransformType::Pointer deformationFieldTransform = DeformationFieldTransformType::New();
    deformationFieldTransform->SetDisplacementField( fieldreader->GetOutput() );
#endif
    typedef itk::ResampleImageFilter<ImageType, ImageType, VectorComponentType >    ResampleFilterType;
    typename ResampleFilterType::Pointer resampleFilter = ResampleFilterType::New();
    resampleFilter->SetInput( image );
    resampleFilter->SetTransform( deformationFieldTransform );
    resampleFilter->SetSize( image->GetLargestPossibleRegion().GetSize() );
    resampleFilter->SetOutputOrigin(  image->GetOrigin() );
    resampleFilter->SetOutputSpacing( image->GetSpacing() );
    resampleFilter->SetOutputDirection( image->GetDirection() );
    resampleFilter->SetDefaultPixelValue( 0 );
    resampleFilter->Update();
    typename ImageType::Pointer outimage = resampleFilter->GetOutput();

    outimage->SetOrigin( origino );
    outimage->SetDirection( directiono );

    typedef itk::ImageFileWriter<  ImageType  > WriterType;
    typename WriterType::Pointer writer = WriterType::New();
    writer->SetInput ( outimage );
    writer->SetFileName(after_image);
    writer->Update();

    return 0;
}



int main(int argc, char *argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }
    if(befor_image == NULL)
    {
        fprintf(stderr, "Unset --src-image\n");
        return -1;
    }
    if(after_image == NULL)
    {
        fprintf(stderr, "Unset --out-image\n");
        return -1;
    }

    if(befor_grid == NULL && after_grid == NULL && deform_field != NULL)
    {
        if(pixel_type == NULL)
            return metoda3<unsigned short>();
        if(strcmp(pixel_type, "u8") == 0)
            return metoda3<unsigned char>();
        if(strcmp(pixel_type, "u16") == 0)
            return metoda3<unsigned short>();
        if(strcmp(pixel_type, "s16") == 0)
            return metoda3<short>();
        if(strcmp(pixel_type, "f32") == 0)
            return metoda3<float>();
        fprintf(stderr, "Error: Incorrect pixel type.\n");
        return -1;
    }

    if(befor_grid == NULL)
    {
        fprintf(stderr, "Unset --src-grid\n");
        return -1;
    }
    if(after_grid == NULL)
    {
        fprintf(stderr, "Unset --dst-grid\n");
        return -1;
    }

    TensionsMode tmode = MODE_AFFIN;
    double params[2];
    params[0] = 0.0;
    params[1] = 0.0;
    Network before_network(params, tmode, NULL);
    if(! before_network.load(befor_grid))
    {
        fprintf(stderr, "Cannot load grid %s\n", befor_grid);
        return -2;
    }
    Network after_network(params, tmode, NULL);
    if(! after_network.load(after_grid))
    {
        fprintf(stderr, "Cannot load grid %s\n", after_grid);
        return -2;
    }

    unsigned int bnc = before_network.getNodesCount();
    unsigned int anc = after_network.getNodesCount();

    if(bnc != anc)
    {
        fprintf(stderr, "Count of nodes mismatch: %i, %i\n", bnc, anc);
        return -3;
    }

    if(pixel_type == NULL)
        return metoda1<unsigned short>(anc, &before_network, &after_network);
    if(strcmp(pixel_type, "u8") == 0)
        return metoda1<unsigned char>(anc, &before_network, &after_network);
    if(strcmp(pixel_type, "u16") == 0)
        return metoda1<unsigned short>(anc, &before_network, &after_network);
    if(strcmp(pixel_type, "s16") == 0)
        return metoda1<short>(anc, &before_network, &after_network);
    if(strcmp(pixel_type, "f32") == 0)
        return metoda1<float>(anc, &before_network, &after_network);
    fprintf(stderr, "Error: Incorrect pixel type.\n");
    return -1;

  //  return metoda2(anc, &before_network, &after_network);
}

// Ref:
// https://itk.org/Wiki/ITK/Examples/Registration/DeformationFieldTransform
// https://itk.org/Doxygen/html/WikiExamples_2Registration_2WarpImageFilter_8cxx-example.html
// https://itk.org/Wiki/ITK/Examples/Registration/WarpImageFilter
// https://itk.org/ITKExamples/src/Filtering/ImageGrid/WarpAnImageUsingADeformationField/Documentation.html

// https://itk.org/Doxygen/html/Examples_2RegistrationITKv4_2ThinPlateSplineWarp_8cxx-example.html#_a7
