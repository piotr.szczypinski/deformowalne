TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = ../deformowalne/Executables
TARGET = meanmask
TEMPLATE = app

SOURCES += meanmask.cpp

#LIBS += -lalglib

include(../qmazda/Pri/config.pri)
include(../qmazda/Pri/itk.pri)
