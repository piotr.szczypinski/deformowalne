/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "axisangl.h"
#include <math.h>
// Makes use of alglib library to compute inverse matrix, singular values
#include <ap.h>
#include <linalg.h>

const double eps = 0.000001;
bool matrixFromAxisAngle(double* matrix, const double* axis, const double* complex)
{
    double c = complex[0];
    double s = complex[1];
    double n = sqrt(c*c + s*s);
    if(n < eps) return false;
    c /= n;
    s /= n;
    double t = 1.0 - c;
    double x = axis[0];
    double y = axis[1];
    double z = axis[2];
    n = sqrt(x*x + y*y + z*z);
    if(n < eps) return false;
    x /= n;
    y /= n;
    y /= n;
    matrix[0] = c + x*x*t;
    matrix[5] = c + y*y*t;
    matrix[10] = c + z*z*t;
    c = x*y*t;
    n = z*s;
    matrix[1] = c + n;
    matrix[4] = c - n;
    c = x*z*t;
    n = y*s;
    matrix[2]= c - n;
    matrix[8] = c + n;
    c = y*z*t;
    n = x*s;
    matrix[6] = c + n;
    matrix[9] = c - n;
    return true;
}

bool averageDirectionPca(double* result, const double* input, const unsigned int dim, const unsigned int count)
{
    alglib::real_2d_array covar;
    alglib::real_1d_array evalus;
    alglib::real_2d_array evctrs;
    covar.setlength(dim, dim);
    evalus.setlength(dim);
    evctrs.setlength(dim, dim);
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) = 0.0;
        }
    }
    for(unsigned int v = 0; v < count; v++)
    {
        for(unsigned int f1 = 0; f1 < dim; f1++)
        {
            double ff1 = input[dim*v + f1];
            for(unsigned int f0 = f1; f0 < dim; f0++)
            {
                double ff0 = input[dim*v + f0];
                covar(f1, f0) += (ff0 * ff1);
            }
        }
    }
    for(unsigned int f1 = 0; f1 < dim; f1++)
    {
        for(unsigned int f0 = f1; f0 < dim; f0++)
        {
            covar(f1, f0) /= count;
        }
    }
    if(! alglib::smatrixevd(covar, dim, 1, true, evalus, evctrs))
    {
        return false;
    }
    for(unsigned int j = 0; j < dim; j++)
    {
        result[j] = evctrs(j, dim-1);
    }
    return true;
}

double dotProduct(const double *wector, const double *vector)
{
    return wector[0]*vector[0] + wector[1]*vector[1] + wector[2]*vector[2];
}

void crossProduct(double* axis, const double *wector, const double *vector)
{
    axis[0] = wector[1]*vector[2] - wector[2]*vector[1];
    axis[1] = wector[2]*vector[0] - wector[0]*vector[2];
    axis[2] = wector[0]*vector[1] - wector[1]*vector[0];
}

void getDirectionsFromMean(double *directions, const double *vectors, const unsigned int count)
{
    double mean[3] = {0.0, 0.0, 0.0};
    double* pd;
    double* pc = (double*) vectors;
    for(unsigned int s = 0; s < count; s++)
    {
        mean[0] += *pc; pc++;
        mean[1] += *pc; pc++;
        mean[2] += *pc; pc++;
    }
    mean[0] /= count;
    mean[1] /= count;
    mean[2] /= count;

    pc = (double*) vectors;
    pd = directions;
    for(unsigned int s = 0; s < count; s++)
    {
        *pd = *pc - mean[0]; pc++;; pd++;
        *pd = *pc - mean[1]; pc++;; pd++;
        *pd = *pc - mean[2]; pc++;; pd++;
    }
}

void getAxesFromVectors(double *axes, const double *vectors1, const double *vectors2, const unsigned int count)
{
    for(unsigned int s = 0; s < count; s++)
    {
        crossProduct(axes+3*s, vectors1+3*s, vectors2+3*s);
    }
}


void getRotationsAsComplex(double* complex, double* axis, double* origin, double* current, const unsigned int count)
{
    double v[3];
    double w[3];
    double c[3];

    for(unsigned int s = 0; s < count; s++)
    {
        crossProduct(v, current + 3*s, axis);
        crossProduct(w, origin + 3*s, axis);
        crossProduct(c, w, v);
        complex[2*s] = dotProduct(w, v);
        complex[2*s + 1] = sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);
    }
}

bool normalizedAverageVectors(double* result, const double* input, const unsigned int dim, const unsigned int count)
{
    for(unsigned int d = 0; d < dim; d++)
        result[d] = 0.0;
    for(unsigned int v = 0; v < count; v++)
    {
        for(unsigned int d = 0; d < dim; d++)
        {
            result[d] = input[dim*v + d];
        }
    }
    double norm = 0.0;
    for(unsigned int d = 0; d < dim; d++)
        norm += (result[d]*result[d]);
    norm = sqrt(norm);
    if(norm < eps)
        return false;
    for(unsigned int d = 0; d < dim; d++)
        result[d] /= norm;
    return true;
}

bool fitOrthogonalAxisAngle(double *matrix, double* buffer, double *qorigin, double *qcurrent, const double *origin, const double *current, const unsigned int count)
{
    double axis[3];
    double rot[2];
    getDirectionsFromMean(qcurrent, current, count);
    getDirectionsFromMean(qorigin, origin, count);

    getAxesFromVectors(buffer, qorigin, qcurrent, count);
    if(! averageDirectionPca(axis, buffer, 3, count))
        return false;
    getRotationsAsComplex(buffer, axis, qorigin, qcurrent, count);
    if(! normalizedAverageVectors(rot, buffer, 2, count))
        return false;
    if(! matrixFromAxisAngle(matrix, axis, rot))
        return false;

//    for(unsigned int i = 0; i < iterations; i++)
//    {
//        getAxesFromVectors(buffer, qorigin, qcurrent);
//        if(! averageDirectionPca(axis, axes, 3, count))
//            return false;
//        getRotationsAsComplex(buffer, axes, qorigin, qcurrent, count);
//        if(! normalizedAverageVectors(rot, buffer, 2, count))
//            return false;
//        if(! matrixFromAxisAngle(matrix, axis, rot))
//            return false;
//    }
    return true;
}

