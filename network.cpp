#include "network.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <ctime>

Network::Network(double parameters[2], TensionsMode tmode, ExternalType* externObject)
{
    dimensions = 3;
    netnodes = NULL;
    extObject = externObject;
    intObject = NULL;
    intBuffer = NULL;

    this->parameters[0] = parameters[0];
    this->parameters[1] = parameters[1];
    this->tmode = tmode;
}
Network::~Network()
{
    intObject->DeInitialize(intBuffer);
    if(netnodes != NULL) delete[] netnodes;
    //if(extObject != NULL) delete extObject;
    if(intObject != NULL) delete intObject;

}

double Network::getNodeCoordinate(unsigned int i, unsigned int d)
{
    return nodes[i].c[d];
}
unsigned int Network::getNodesCount(void)
{
    return nodes.size();
}


bool Network::createNetwork()
{
    unsigned int n;
    if(nodes.size() <= 0 || connections.size() <= 0)
        return false;
    if(netnodes != NULL) delete[] netnodes;
    //if(extObject != NULL) delete[] extObject;
    if(intObject != NULL) delete[] intObject;

    netnodes = new Node*[nodes.size()];

//    unsigned int max = 0;
//    for(n = 0; n < nodes.size(); n++)
//    {
//        if(max < nodes[n].connections) max = nodes[n].connections;
//    }

    //extObject = new ExternalType;
    intObject = new Internal(tmode, dimensions, nodes.size());
    Node** nos = new Node*[nodes.size()];

    for(n = 0; n < nodes.size(); n++)
    {
        netnodes[n] = new Node(nodes[n].c, dimensions, parameters, intObject);
    }
    for(n = 0; n < nodes.size(); n++)
    {
        unsigned int count = 0;
        for(unsigned int c = 0; c < connections.size() && count < nodes.size(); c++)
        {
            if(connections[c].c[0] == n)
            {
                nos[count] = netnodes[connections[c].c[1]];
                count++;
            }
            if(connections[c].c[1] == n)
            {
                nos[count] = netnodes[connections[c].c[0]];
                count++;
            }
        }
        netnodes[n]->Initialize(nos, count);
    }
    delete[] nos;

    unsigned int s = dimensions*nodes.size();
    double* neighborsCoordinates = new double[s];
    double* rn = neighborsCoordinates;
    for(n = 0; n < nodes.size(); n++)
    {
        netnodes[n]->getRefCoordinates(rn);
        rn += dimensions;
    }
    intObject->Initialize(&intBuffer, neighborsCoordinates, nodes.size());
//    extObject->Initialize(neighborsCoordinates, nodes.size());

    delete[] neighborsCoordinates;
    return true;
}


void Network::iterateRigid(void)
{
    unsigned int ns = nodes.size();
    unsigned int s = dimensions*nodes.size();
    double* neighborsCoordinates = new double[2*s];
    unsigned int i, d;
    double* nn = neighborsCoordinates;
    double* rn = neighborsCoordinates+s;
    for(i = 0; i < ns; i++)
    {
        netnodes[i]->getCoordinates(nn);
        netnodes[i]->getRefCoordinates(rn);
        nn += dimensions;
        rn += dimensions;
    }

    intObject->FindTransform(neighborsCoordinates+s, neighborsCoordinates, intBuffer, ns);
    nn = neighborsCoordinates;
    rn = neighborsCoordinates+s;
    for(i = 0; i < ns; i++)
    {
        intObject->GetShift(netnodes[i]->intVector, rn, nn);
        nn += dimensions;
        rn += dimensions;
    }
    extObject->findTransform(neighborsCoordinates+s, neighborsCoordinates, nodes.size());
    nn = neighborsCoordinates;
    for(i = 0; i < ns; i++)
    {
        for(d = 0; d < dimensions; d++)
        {
            netnodes[i]->extVector[d] = *nn;
            //netnodes[i]->extVector[d] = 0.0;
            nn++;
        }
        netnodes[i]->UpdateLocation();
    }
    delete[] neighborsCoordinates;
}

void Network::iterateFlexible(void)
{
    unsigned int n, d;
    unsigned int ns = nodes.size();
    for(n = 0; n < ns; n++)
    {
        netnodes[n]->ComputeVectors();
    }
    unsigned int s = dimensions*nodes.size();
    double* neighborsCoordinates = new double[2*s];

    double* nn = neighborsCoordinates;
    double* rn = neighborsCoordinates+s;
    for(n = 0; n < ns; n++)
    {
        netnodes[n]->getCoordinates(nn);
        netnodes[n]->getRefCoordinates(rn);
        nn += dimensions;
        rn += dimensions;
    }
    extObject->findTransform(neighborsCoordinates+s, neighborsCoordinates, nodes.size());
    nn = neighborsCoordinates;
    for(n = 0; n < ns; n++)
    {
        for(d = 0; d < dimensions; d++)
        {
            netnodes[n]->extVector[d] = *nn;
            //netnodes[n]->extVector[d] = 0.0;
            nn++;
        }
        netnodes[n]->UpdateLocation();
    }
    delete[] neighborsCoordinates;
}


//void Network::deform(void)
//{
//    for(unsigned int n = 0; n < nodes.size(); n++)
//    {
//        double c[4];
//        netnodes[n]->getCoordinates(c);
//        for(unsigned int k = 0; k < dimensions; k++)
//        {
//            c[k] += (double)(rand()%1001-500)/1000;
//        }
//        netnodes[n]->setCoordinates(c);
//    }
//}

void Network::generateRandom(float subtract, float divide, float distance)
{
    srand(time(NULL));
    nodes.clear();
    connections.clear();
    double spacing[4];
    unsigned int size[4];
    PixelType* ptr = extObject->getFixedInfo(size, spacing);

    if(ptr == NULL) return;
    for(unsigned int z = 0; z < size[2]; z++)
    {
        for(unsigned int y = 0; y < size[1]; y++)
        {
            for(unsigned int x = 0; x < size[0]; x++)
            {
                float p = *ptr;
                p = (p - subtract) / divide;
                if(rand() < p*RAND_MAX)
                {
                    NetworkNode newnode;
                    newnode.connections = 0;
                    newnode.c[0] = x * spacing[0];
                    newnode.c[1] = y * spacing[1];
                    newnode.c[2] = z * spacing[2];
                    nodes.push_back(newnode);
                }
                ptr++;
            }
        }
    }
    unsigned int ns = nodes.size();
    for(unsigned int n = 0; n < ns-1; n++)
    {
        for(unsigned int s = n+1; s < ns; s++)
        {
            double dd;
            dd = nodes[n].c[0]-nodes[s].c[0];
            dd *= dd;
            double d = dd;
            dd = nodes[n].c[1]-nodes[s].c[1];
            dd *= dd;
            d += dd;
            dd = nodes[n].c[2]-nodes[s].c[2];
            dd *= dd;
            d += dd;
            d = sqrt(d);
            if(d < distance)
            {
                NetworkConnection newcon;
                newcon.c[0] = n;
                newcon.c[1] = s;
                nodes[n].connections++;
                nodes[s].connections++;
                connections.push_back(newcon);
            }
        }
    }
    createNetwork();
}

bool Network::load(std::ifstream* file)
{
    std::string inputstring;
    *file >> inputstring;

    if(inputstring == "@TreeSkeleton2014_Internal")
    {
        unsigned int NumberOfNodes;
        unsigned int NumberOfBranches;

        *file >> inputstring;
        if(inputstring == "@NumberOfDimensions")
            *file >> dimensions;
        else
            dimensions = 3;

        if(dimensions < 2 || dimensions > 4)
        {
            dimensions = 3;
            return false;
        }
        if(inputstring != "@NumberOfAllNodes")
            return false;
        *file >> NumberOfNodes;
        if(NumberOfNodes <= 0)
            return false;
        for(unsigned int n = 0; n < NumberOfNodes; n++)
        {
            NetworkNode newnode;
            newnode.connections = 0;
            for(unsigned int d = 0; d < dimensions; d++)
                *file >> newnode.c[d];
            *file >> newnode.connections >> newnode.radius;
            nodes.push_back(newnode);
        }
        *file >> inputstring;
        if(inputstring != "@NumberOfBranches")
            return false;
        *file >> NumberOfBranches;

        if(NumberOfBranches <= 0)
            return false;
        for(unsigned int n = 0; n < NumberOfBranches; n++)
        {
            unsigned int NumberOfBranchNodes;
            *file >> NumberOfBranchNodes;
            if(NumberOfBranchNodes < 2)
                return false;

            NetworkConnection newconnection;

            *file >> newconnection.c[0];
            if(newconnection.c[0] >= NumberOfNodes)
                return false;

            for(unsigned int n = 1; n < NumberOfBranchNodes; n++)
            {
                *file >> newconnection.c[1];
                if(newconnection.c[1] >= NumberOfNodes)
                    return false;
                connections.push_back(newconnection);
                nodes[newconnection.c[0]].connections++;
                nodes[newconnection.c[1]].connections++;
                newconnection.c[0] = newconnection.c[1];
            }
        }
    }
    createNetwork();
    return true;
}

bool Network::load(const char *fileName)
{
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
        return false;
    if (!file.good())
        return false;
    if(! load(&file))
    {
        file.close();
        return false;
    }
    file.close();
    return true;
}

bool Network::save(std::ofstream* file, bool reference)
{
    *file << "@TreeSkeleton2014_Internal"<< std::endl;
    *file << "@NumberOfAllNodes " << nodes.size() << std::endl;
    for(unsigned int n = 0; n < nodes.size(); n++)
    {
        if(reference || netnodes == NULL)
            *file << "\t" << nodes[n].c[0] << " " << nodes[n].c[1] << " " << nodes[n].c[2] << " " << nodes[n].connections << " "<< 0.1 << std::endl;
        else
        {
            double c[4];
            netnodes[n]->getCoordinates(c);
            *file << "\t" << c[0] << " " << c[1] << " " << c[2] << " " << nodes[n].connections << " "<< 0.1 << std::endl;
        }
    }
    *file << "@NumberOfBranches " << connections.size() << std::endl;
    for(std::vector<NetworkConnection>::iterator b = connections.begin(); b != connections.end(); ++b)
    {
        *file << "\t" << 2 << " " << b->c[0] << " " << b->c[1] << std::endl;
    }
    return true;
}

bool Network::save(const char *fileName, bool reference)
{
    std::ofstream file;
    file.open(fileName);
    if (!file.is_open()) return false;
    if (!file.good()) return false;
    if(! save(&file, reference))
    {
        file.close();
        return false;
    }
    file.close();
    return true;
}














void Network::deform(void)
{
//    const double jt[12] =
//    {
//        1.707,  1.707,  0,    0,
//        -1.707,  1.707,  0,    0,
//        0,  0,  1,    0,
//    };
//    for(unsigned int n = 0; n < nodes.size(); n++)
//    {
//        double c[4];
//        double cc[4];
//        netnodes[n]->getCoordinates(c);
//        for(unsigned int k = 0; k < 3; k++)
//        {
//            double v = 0.0;
//            for(unsigned int d = 0; d < 3; d++)
//            {
//                v += c[d] * jt[k*4 + d];
//            }
//            v += jt[k*4 + 3];
//            cc[k] = v + (double)(rand()%1001-500)/200;
//        }
//        netnodes[n]->setCoordinates(cc);
//    }

    for(unsigned int n = 0; n < nodes.size(); n++)
    {
        double c[4];
        netnodes[n]->getCoordinates(c);
        for(unsigned int k = 0; k < dimensions; k++)
        {
            c[k] += (double)(rand()%1001-500)/1000;
        }
        netnodes[n]->setCoordinates(c);
    }



}


void Network::generateRandom(double boundingBox[6], double density, double linkDistance)
{
    srand(274536);
    nodes.clear();
    connections.clear();

    for(int z = boundingBox[2]; z < boundingBox[5]; z++)
    {
        for(int y = boundingBox[1]; y < boundingBox[4]; y++)
        {
            for(int x = boundingBox[0]; x < boundingBox[3]; x++)
            {
                if(rand() < density*RAND_MAX)
                {
                    NetworkNode newnode;
                    newnode.connections = 0;
                    newnode.c[0] = x;
                    newnode.c[1] = y;
                    newnode.c[2] = z;
                    nodes.push_back(newnode);
                }
            }
        }
    }

    unsigned int ns = nodes.size();
    for(unsigned int n = 0; n < ns-1; n++)
    {
        for(unsigned int s = n+1; s < ns; s++)
        {
            double dd;
            dd = nodes[n].c[0]-nodes[s].c[0];
            dd *= dd;
            double d = dd;
            dd = nodes[n].c[1]-nodes[s].c[1];
            dd *= dd;
            d += dd;
            dd = nodes[n].c[2]-nodes[s].c[2];
            dd *= dd;
            d += dd;
            d = sqrt(d);

            if(d < linkDistance)
            {
                NetworkConnection newcon;
                newcon.c[0] = n;
                newcon.c[1] = s;
                nodes[n].connections++;
                nodes[s].connections++;
                connections.push_back(newcon);
            }
        }
    }
    createNetwork();
}


void Network::generateRandom(void)
{
    int radius = 9;
    int density = 20;
    int distance = 5;

    srand(27386);
    nodes.clear();
    connections.clear();

    for(int z = -radius; z < radius; z++)
    {
        for(int y = -radius; y < radius; y++)
        {
            for(int x = -radius; x < radius; x++)
            {
                if(x*x+y*y+z*z <= radius*radius)
                {
                    if(rand()%1000 < density)
                    {
                        NetworkNode newnode;
                        newnode.connections = 0;
                        newnode.c[0] = x;
                        newnode.c[1] = y;
                        newnode.c[2] = z;
                        nodes.push_back(newnode);
                    }
                }
            }
        }
    }

    for(unsigned int n = 0; n < nodes.size()-1; n++)
    {
        for(unsigned int s = n+1; s < nodes.size(); s++)
        {
            double dd;
            dd = nodes[n].c[0]-nodes[s].c[0];
            dd *= dd;
            double d = dd;
            dd = nodes[n].c[1]-nodes[s].c[1];
            dd *= dd;
            d += dd;
            dd = nodes[n].c[2]-nodes[s].c[2];
            dd *= dd;
            d += dd;
            d = sqrt(d);

            if(d < distance)
            {
                NetworkConnection newcon;
                newcon.c[0] = n;
                newcon.c[1] = s;
                nodes[n].connections++;
                nodes[s].connections++;
                connections.push_back(newcon);
            }
        }
    }
    createNetwork();
}
