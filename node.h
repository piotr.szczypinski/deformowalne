/*
 * Computes motion of node
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NODE_H
#define NODE_H

#include "internal.h"
#include "external.h"
//#include "network.h"

class Node
{
public:
    Node(const double* coordinates,
         const unsigned int dimensions,
         const double *parameters,
         const Internal *intObject);
    ~Node();
    void Initialize(Node** neighborPointers,
                    const unsigned int neighborsCount);
    void ComputeVectors(void);
    void UpdateLocation(void);
    void getRefCoordinates(double *coords);
    void getCoordinates(double *coords);
    void setCoordinates(double *coords);

    double* intVector;
    double* extVector;

private:
    double* coordinates;
    double* refCoordinates;
    double intWeight;
    double extWeight;

    Internal* intObject;
    void* intBuffer;

    unsigned int dimensions;
    unsigned int neighborsCount;
    Node** neighborPointers;
};

#endif // NODE_H
