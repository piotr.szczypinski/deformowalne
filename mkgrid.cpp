/*
 * Registers images elastically by 3D grid deformation
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "network.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include "../qmazda/MzShared/getoption.h"

char* prob_image = NULL;
char* output_grid = NULL;
float subtract = 0;
float divide = std::numeric_limits<float>::quiet_NaN();
float radius = std::numeric_limits<float>::quiet_NaN();
bool isprinthelp = false;

int scan_parameters_local(int argc, char* argv[])
{
    int argi = 1;
    while(argi < argc)
    {
        GET_STRING_OPTION("-p", "--prob-image", prob_image)
        else GET_STRING_OPTION("-o", "--outpt-grid", output_grid)
        else GET_FLOAT_OPTION("-s", "--subtract", subtract)
        else GET_FLOAT_OPTION("-d", "--divide", divide)
        else GET_FLOAT_OPTION("-r", "--radius", radius)
        else GET_NOARG_OPTION("/?", "--help", isprinthelp, true)
        else return argi;
        argi++;
    }
    return 0;
}

void printhelp_local(char* name)
{
    //------------------------------------------------------------------------------------------v
    printf("Usage: %s [OPTION]...\n", name);
    printf("Creates a grid from probabbility image\n");
    printf("2018 by Piotr M. Szczypinski\n");
    printf("Compilation date and time: %s %s\n", __DATE__, __TIME__);
    printf("Options:\n");
    printf("  -p, --prob-image <file>  Load image from <file>.\n");
    printf("  -o, --outpt-grid <file>  Save resulting grid to <file>.\n");
    printf("  -s, --subtract <fp>      Subtract this from probability image\n");
    printf("  -d, --divide <fp>        Divide probability by this number\n");
    printf("  -r, --radius <fp>        Radius for node connection\n");
    printf("  /?, --help               Display this help and exit\n\n");
}

int main(int argc, char *argv[])
{
    int ret = scan_parameters_local(argc, argv);
    if(isprinthelp || argc <= 1)
    {
        printhelp_local(argv[0]);
        return 1;
    }
    if(ret != 0)
    {
        if(ret < argc) fprintf(stderr, "Incorrect operand: %s\n", argv[ret]);
        fprintf(stderr, "Try --help for more information.\n");
        return ret;
    }

    if(prob_image == NULL)
    {
        fprintf(stderr, "Unset --prob-image\n");
        return -1;
    }
    if(output_grid == NULL)
    {
        fprintf(stderr, "Unset --output-grid\n");
        return -1;
    }
    if(!(subtract == subtract))
    {
        fprintf(stderr, "Unset --subtract\n");
        return -1;
    }
    if(!(divide == divide))
    {
        fprintf(stderr, "Unset --divide\n");
        return -1;
    }
    if(!(radius == radius))
    {
        fprintf(stderr, "Unset --radius\n");
        return -1;
    }

    ExternalType* ext = new ExternalType(MODE_GRADMAD);
    ext->setRadi(1, radius);
    if(! ext->loadFixed(prob_image))
    {
        fprintf(stderr, "Cannot load probability map %s\n", prob_image);
        return -2;
    }

    double params[2];
    params[0] = 0;
    params[1] = 0;

    Network network(params, MODE_AFFIN, ext);
    network.generateRandom(subtract, divide, radius);
    if(! network.save(output_grid))
    {
        fprintf(stderr, "Cannot save grid %s\n", output_grid);
        return -3;
    }
    return 0;
}
