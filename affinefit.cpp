/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Refs:
// Spath, Helmuth. "Fitting affine and orthogonal transformations between two sets of points." Mathematical Communications 9.1 (2004): 27-34.
// https://www.tol-project.org/svn/tolp/OfficialTolArchiveNetwork/AlgLib/CppTools/source/alglib/manual.cpp.html

#include "affinefit.h"

// Makes use of alglib library to compute inverse matrix, singular values, determinant
#include <ap.h>
#include <linalg.h>

bool affineFitOrigin(double* result, double *wmean, const double* origin, const unsigned int dimensions, const unsigned int count)
{
    alglib::ae_int_t info;
    alglib::matinvreport rep;
    alglib::real_2d_array matrix;

    if(count < dimensions) return false;
    matrix.setlength(dimensions+1, dimensions+1);
    for(unsigned int d = 0; d < dimensions; d++)
    {
        double sum = 0.0;
        for(unsigned int s = 0; s < count; s++)
        {
            sum += origin[s*dimensions + d] * origin[s*dimensions + d];
        }
        matrix(d, d) = sum;
    }
    matrix(dimensions, dimensions) = count;

    for(unsigned int d = 0; d < dimensions; d++)
    {
        for(unsigned int i = d + 1; i < dimensions; i++)
        {
            double sum = 0.0;
            for(unsigned int s = 0; s < count; s++)
            {
                sum += (origin[s*dimensions + d] * origin[s*dimensions + i]);
            }
            matrix(d, i) = sum;
            matrix(i, d) = sum;
        }
    }

    for(unsigned int d = 0; d < dimensions; d++)
    {
        double sum = 0.0;
        for(unsigned int s = 0; s < count; s++)
        {
            sum += origin[s*dimensions + d];
        }
        matrix(d, dimensions) = sum;
        matrix(dimensions, d) = sum;
        if(wmean != NULL) wmean[d] = sum / count;
    }

    alglib::rmatrixinverse(matrix, info, rep);
    if(info <= 0) return false;

    double* p = result;

    for(unsigned int d = 0; d <= dimensions; d++)
    {
        for(unsigned int i = d; i <= dimensions; i++, p++)
        {
            *p = matrix(i, d);
        }
    }
    return true;
}

void affineFitCurrent(double *result, double *vmean, double *buffer, const double* matrix, const double* origin, const double *current, const unsigned int dimensions, const unsigned int count)
{
    for(unsigned int k = 0; k < dimensions; k++)
    {
        double sum;
        for(unsigned int d = 0; d < dimensions; d++)
        {
            sum = 0.0;
            for(unsigned int s = 0; s < count; s++)
            {
                sum += origin[s*dimensions + d] * current[s*dimensions + k];
            }
            buffer[d] = sum;
        }
        sum = 0.0;
        for(unsigned int s = 0; s < count; s++)
        {
            sum += current[s*dimensions + k];
        }
        buffer[dimensions] = sum;
        if(vmean != NULL) vmean[k] = sum / count;

        for(unsigned int d = 0; d <= dimensions; d++)
        {
            unsigned int i = 0;
            double* p = (double*) matrix + d;
            unsigned int dd = dimensions;
            double* v = buffer;

            sum = 0.0;
            for(; i < d; i++)
            {
                sum += (*p * *v);
                p += dd;
                dd--;
                v++;
            }
            for(; i <= dimensions; i++)
            {
                sum += (*p * *v);
                p++;
                v++;
            }
            result[k*(dimensions+1) + d] = sum;
        }
    }
}

void affineRecomputeTranslation(double *matrix, const double *wmean, const double *vmean, const unsigned int dimensions)
{
    for(unsigned int k = 0; k < dimensions; k++)
    {
        double sum = 0.0;
        for(unsigned int d = 0; d < dimensions; d++)
        {
            sum += (matrix[k*(dimensions+1) + d] * wmean[d]);
        }
        matrix[k*(dimensions+1) + dimensions] = (vmean[k] - sum);
    }
}

void affineRecomputeSizeTranslation(double *matrix, double *wmean, double *vmean, const double* origin, const double *current, const unsigned int dimensions, const unsigned int count)
{
    double det = 1.0;
    for(unsigned int d = 0; d < dimensions; d++)
    {
        double sumv = 0.0;
        double sumsv = 0.0;
        double sumw = 0.0;
        double sumsw = 0.0;
        for(unsigned int s = 0; s < count; s++)
        {
            double v = current[dimensions*s + d];
            sumv += v;
            sumsv += (v*v);
            v = origin[dimensions*s + d];
            sumw += v;
            sumsw += (v*v);
        }
        sumv /= count;
        sumsv /= count;
        sumw /= count;
        sumsw /= count;
        sumsv -= (sumv*sumv);
        sumsw -= (sumw*sumw);
        vmean[d] = sumv;
        wmean[d] = sumw;
        det *= sumsv/sumsw;
    }
    det = pow(det, 1.0/(dimensions*2.0));
    for(unsigned int k = 0; k < dimensions; k++)
    {
        for(unsigned int d = 0; d < dimensions; d++)
        {
            matrix[k*(dimensions+1) + d] *= det;
        }
    }
    affineRecomputeTranslation(matrix, wmean, vmean, dimensions);
}

void affineToProcrustes(double* result, const double *affine, const double *wmean, const double *vmean, const unsigned int dimensions)
{
    alglib::real_2d_array affn;
    alglib::real_1d_array eigv;
    alglib::real_2d_array u;
    alglib::real_2d_array vt;

    affn.setlength(dimensions, dimensions);
    eigv.setlength(dimensions);
    u.setlength(dimensions, dimensions);
    vt.setlength(dimensions, dimensions);

    for(unsigned int k = 0; k < dimensions; k++)
    {
        for(unsigned int d = 0; d < dimensions; d++)
        {
            affn(k, d) = affine[k*(dimensions+1) + d];
        }
    }
    if(alglib::rmatrixsvd(affn, dimensions, dimensions, 2, 2, 2, eigv, u, vt))
    {
        double det = 1.0;
        for(unsigned int d = 0; d < dimensions; d++)
        {
            det *= eigv[d];
        }
        det = fabs(det);
        det = pow(det, 1.0/dimensions);
        alglib::rmatrixgemm(dimensions, dimensions, dimensions, 1.0, u,0,0,0, vt,0,0,0, 0, affn,0,0);
        for(unsigned int k = 0; k < dimensions; k++)
        {
            for(unsigned int d = 0; d < dimensions; d++)
            {
                result[k*(dimensions+1) + d] = affn(k, d)*det;
            }
        }
    }
    affineRecomputeTranslation(result, wmean, vmean, dimensions);
}

void affineToUnimodular(double* result, const double *affine, const double *wmean, const double *vmean, const unsigned int dimensions)
{
    alglib::real_2d_array affn;
    affn.setlength(dimensions, dimensions);
    for(unsigned int k = 0; k < dimensions; k++)
    {
        for(unsigned int d = 0; d < dimensions; d++)
        {
            affn(k, d) = affine[k*(dimensions+1) + d];
        }
    }
    double det = alglib::rmatrixdet(affn);
    det = pow(fabs(det), (double)1.0/dimensions);
    if(det > 0.0)
    {
        det = 1.0/det;
        for(unsigned int k = 0; k < dimensions; k++)
        {
            for(unsigned int d = 0; d < dimensions; d++)
            {
                result[k*(dimensions+1) + d] = affine[k*(dimensions+1) + d] * det;
            }
        }
    }
    affineRecomputeTranslation(result, wmean, vmean, dimensions);
}

void affineToOrthogonal(double* result, const double *affine, const double *wmean, const double *vmean, const unsigned int dimensions)
{
    alglib::real_2d_array affn;
    alglib::real_1d_array eigv;
    alglib::real_2d_array u;
    alglib::real_2d_array vt;

    affn.setlength(dimensions, dimensions);
    eigv.setlength(dimensions);
    u.setlength(dimensions, dimensions);
    vt.setlength(dimensions, dimensions);

    for(unsigned int k = 0; k < dimensions; k++)
    {
        for(unsigned int d = 0; d < dimensions; d++)
        {
            affn(k, d) = affine[k*(dimensions+1) + d];
        }
    }
    if(alglib::rmatrixsvd(affn, dimensions, dimensions, 2, 2, 2, eigv, u, vt))
    {
        alglib::rmatrixgemm(dimensions, dimensions, dimensions, 1.0, u,0,0,0, vt,0,0,0, 0, affn,0,0);
        for(unsigned int k = 0; k < dimensions; k++)
        {
            for(unsigned int d = 0; d < dimensions; d++)
            {
                result[k*(dimensions+1) + d] = affn(k, d);
            }
        }
    }
    affineRecomputeTranslation(result, wmean, vmean, dimensions);
}
