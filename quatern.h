/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QUATERN_H
#define QUATERN_H

/**
 * @brief quaternionGetDirections
 * @param directions
 * @param mean
 * @param vectors
 * @param dim
 * @param count
 */
void quaternionGetDirections(double *directions, double* mean, const double *vectors, const unsigned int dim, const unsigned int count);

/**
 * @brief quaternionFromVectors
 * @param quat - output quaternion representing rotation between the two vectors (allocate: new double[4])
 * @param norm - output noralization value (|w|*|v|)
 * @param wector - origin (reference) vector
 * @param vector - current (transformed) vector
 */
void quaternionFromVectors(double* quat, double* norm, const double *wector, const double *vector);

/**
 * @brief quaternionFromAxisAndAngle
 * @param quat - output quaternion (allocate: new double[4])
 * @param axis - axis vector
 * @param angle - angle
 */
void quaternionFromAxisAndAngle(double* quat, const double* axis, const double angle);

/**
 * @brief quaternionPcaAverage
 * @param quat - output quaternion (allocate: new double[4])
 * @param input - table of quaternions
 * @param weights - table of weights for averaging
 * @param count - count of elements in the two tables
 * @return - true on success
 */
bool quaternionPcaAverage(double* output, const double *input, const unsigned int dim, const unsigned int count);

/**
 * @brief quaternionToRotationMatrix
 * @param mat - output rotation matrix (allocate: new double[4*3])
 * @param quat - input quaternion
 */
void quaternionToRotationMatrix(double* mat, const double* quat);

/**
 * @brief quaternionFitOrthogonalIterative
 * @param matrix
 * @param quaternion
 * @param quaternions
 * @param qrotated
 * @param qcurrent
 * @param origin
 * @param current
 * @param count
 * @param iterations
 * @return
 */
bool quaternionFitOrthogonalIterative(double *matrix, double *quaternion, double *quaternions, double *qrotated, double *qcurrent, const double *origin, const double *current, const unsigned int count, const unsigned int iterations);

bool quaternionFitOrthogonal(double *matrix, double *quaternion, double *quaternions, const double *qrotated, const double *qcurrent, const unsigned int count);


/**
 * @brief complexFitOrthogonalFromDirections
 * @param average
 * @param origin
 * @param current
 * @param count
 * @return
 */
bool complexFitOrthogonalFromDirections(double* average, const double *origin, const double *current, const unsigned int count);

/**
 * @brief complexToRotationMatrix
 * @param mat
 * @param cplx
 */
void complexToRotationMatrix(double* mat, const double* cplx);

/**
 * @brief complexFitOrthogonalIterative
 * @param matrix
 * @param complex
 * @param qorigin
 * @param qcurrent
 * @param origin
 * @param current
 * @param count
 * @return
 */
bool complexFitOrthogonal(double *matrix, double *complex, double *qorigin, double *qcurrent, const double *origin, const double *current, const unsigned int count);

bool complexFitOrthogonal(double *matrix, double *complex, const double *qorigin, const double *qcurrent, const unsigned int count);

#endif // QUATERN_H
