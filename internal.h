/*
 * Computes tensions by transforming reference nodes
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TENSIONS_H
#define TENSIONS_H

enum TensionsMode {MODE_AFFIN, //Affine: translation allowed, resizing allowed, rotation allowed, shear allowed
                   MODE_PROCR, //Procrustes: translation allowed, resizing allowed, rotation allowed, shear restricted
                   MODE_UNIMO, //Unimodal: translation allowed, resizing restricted, rotation allowed, shear allowed
                   MODE_ORTHO, //Orthonormal: translation allowed, resizing restricted, rotation allowed, shear restricted
                   MODE_ROTAT, //Complex/Quaternion: translation allowed, resizing restricted, rotation allowed, shear restricted
                   MODE_ROTSZ  //Complex/Quaternion: translation allowed, resizing allowed, rotation allowed, shear restricted
                  };

class Internal
{
public:
    Internal(const TensionsMode configMode, const unsigned int dimensions, const unsigned int maxCount);
    bool Initialize(void** buffer, const double* referenceCoordinates, const unsigned int count);
    void DeInitialize(void* buffer);
    void FindTransform(const double* referenceCoordinates, const double* currentCoordinates, const void* buffer, const unsigned int count);
    void GetShift(double* shift, const double* referenceCoordinates, const double* currentCoordinates);
private:
    unsigned int dims;
    TensionsMode mode;
    double* jtf;
    double* vbuf;
    double* buf;
};

#endif // TENSIONS_H
