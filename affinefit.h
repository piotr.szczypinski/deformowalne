/*
 * Fitting transformation between two sets of points
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AFFINEFIT_H
#define AFFINEFIT_H

/**
 * @brief affineFitOrigin
 * @param result - output intermediate matrix upper triangle (allocate: new double[((dims+1)*(dims+1)+dims+1)/2])
 * @param wmean - output mean coordinate of original nodes (new double[dims])
 * @param origin - input vector of origin (reference) coordinates of nodes
 * @param dimensions - input number of space dimensions
 * @param count - input count of reference nodes
 * @return true on success
 */
bool affineFitOrigin(double* result, double *wmean, const double* origin, const unsigned int dimensions, const unsigned int count);

/**
 * @brief affineFitCurrent
 * @param result - output affine transformation matrix and translation (allocate: new double[(dims+1)*dims])
 * @param vmean - output mean coordinate of current (transformed) nodes (allocate: new double[dims])
 * @param buffer - waste buffer required for computations (new double[dims+1])
 * @param matrix - input intermediate matrix upper triangle resulted from affineFitOrigin()
 * @param origin - input vector of origin (reference) coordinates of nodes
 * @param current - input vector of current (transformed) coordinates of nodes
 * @param dimensions - input number of space dimensions
 * @param count - input count of reference nodes
 */
void affineFitCurrent(double* result, double *vmean, double *buffer, const double *matrix, const double *origin, const double *current, const unsigned int dimensions, const unsigned int count);

/**
 * @brief affineRecomputeTranslation
 * @param matrix
 * @param wmean
 * @param vmean
 * @param dimensions
 */
void affineRecomputeTranslation(double *matrix, const double *wmean, const double *vmean, const unsigned int dimensions);

void affineRecomputeSizeTranslation(double *matrix, double *wmean, double *vmean, const double* origin, const double *current, const unsigned int dimensions, const unsigned int count);

/**
 * @brief affineToProcrustes
 * @param result - output matrix and translation of closest Procrust transformation (may be the same as affine or allocate: new double[(dims+1)*dims])
 * @param affine - input affine transformation matrix and translation
 * @param wmean - input mean coordinate of original nodes from affineFitOrigin()
 * @param vmean - input mean coordinate of current (transformed) nodes from affineFitCurrent()
 * @param dimensions - input number of space dimensions
 */
void affineToProcrustes(double *result, const double* affine, const double *wmean, const double *vmean, const unsigned int dimensions);

/**
 * @brief affineToOrthogonal
 * @param result - output matrix and translation of closest orthogonal transformation (may be the same as affine or allocate: new double[(dims+1)*dims])
 * @param affine - input affine transformation matrix and translation
 * @param wmean - input mean coordinate of original nodes from affineFitOrigin()
 * @param vmean - input mean coordinate of current (transformed) nodes from affineFitCurrent()
 * @param dimensions - input number of space dimensions
 */
void affineToOrthogonal(double *result, const double* affine, const double *wmean, const double *vmean, const unsigned int dimensions);

void affineToUnimodular(double* result, const double *affine, const double *wmean, const double *vmean, const unsigned int dimensions);

#endif // AFFINEFIT_H
