/*
 * Computes motion of node
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "node.h"

Node::Node(const double* coordinates,
           const unsigned int dimensions,
           const double* parameters,
           const Internal* intObject)
{
    unsigned int i;
    this->dimensions = dimensions;
    this->coordinates = new double[4*dimensions];
    this->refCoordinates = this->coordinates+dimensions;
    this->intVector = this->refCoordinates+dimensions;
    this->extVector = this->intVector+dimensions;

    for(i = 0; i < dimensions; i++)
        this->coordinates[i] = this->refCoordinates[i] = coordinates[i];
    this->intObject = (Internal*) intObject;
    this->intWeight = parameters[0];
    this->extWeight = parameters[1];
}

Node::~Node()
{
    intObject->DeInitialize(intBuffer);
    delete[] neighborPointers;
    delete[] coordinates;
}

void Node::Initialize(Node** neighborPointers,
                      const unsigned int neighborsCount)
{
    unsigned int i;

    this->neighborsCount = neighborsCount;
    this->neighborPointers = new Node*[neighborsCount];
    for(i = 0; i < neighborsCount; i++)
        this->neighborPointers[i] = (Node*) neighborPointers[i];

    double* neighborsCoordinates = new double[dimensions*(neighborsCount+1)];
    getRefCoordinates(neighborsCoordinates);
    for(i = 0; i < neighborsCount; i++)
        neighborPointers[i]->getRefCoordinates(neighborsCoordinates+dimensions*(i+1));
    intObject->Initialize(&intBuffer, neighborsCoordinates, neighborsCount+1);
    delete[] neighborsCoordinates;
}

void Node::ComputeVectors(void)
{
    unsigned int s = dimensions*(neighborsCount+1);
    double* neighborsCoordinates = new double[2*s];
    unsigned int i;

    double* nn = neighborsCoordinates;
    double* rn = neighborsCoordinates+s;
    getCoordinates(nn);
    getRefCoordinates(rn);
    for(i = 0; i < neighborsCount; i++)
    {
        nn += dimensions;
        rn += dimensions;
        neighborPointers[i]->getCoordinates(nn);
        neighborPointers[i]->getRefCoordinates(rn);
    }
    intObject->FindTransform(neighborsCoordinates+s, neighborsCoordinates, intBuffer, neighborsCount+1);
    intObject->GetShift(intVector, neighborsCoordinates+s, neighborsCoordinates);

//    extObject->GetShift(extVector, neighborsCoordinates+s, neighborsCoordinates);
    delete[] neighborsCoordinates;
}

void Node::UpdateLocation(void)
{
    unsigned int i;
    for(i = 0; i < dimensions; i++)
        coordinates[i] += (intVector[i]*intWeight + extVector[i]*extWeight);
}

void Node::getCoordinates(double* coords)
{
    unsigned int i;
    for(i = 0; i < dimensions; i++)
        coords[i] = coordinates[i];
}

void Node::setCoordinates(double* coords)
{
    unsigned int i;
    for(i = 0; i < dimensions; i++)
        coordinates[i] = coords[i];
}


void Node::getRefCoordinates(double* coords)
{
    unsigned int i;
    for(i = 0; i < dimensions; i++)
        coords[i] = refCoordinates[i];
}
