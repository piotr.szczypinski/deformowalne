/*
 * Computes tensions by transforming reference nodes
 *
 * Sponsored by National Science Centre Poland (Narodowe Centrum Nauki)
 * grant  ST7/OPUS-8
 *
 * Copyright 2017  Piotr M. Szczypiński <piotr.szczypinski@p.lodz.pl>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>

#include "affinefit.h"
#include "quatern.h"
#include "internal.h"

Internal::Internal(const TensionsMode configMode, const unsigned int dimensions, const unsigned int maxCount)
{
    dims = dimensions;
    mode = configMode;

    switch(mode)
    {
    case MODE_AFFIN:
    case MODE_PROCR:
    case MODE_ORTHO:
    case MODE_UNIMO:
        jtf = new double[(dims+1)*dims];
        vbuf = new double[dims];
        buf = new double[dims+1];
        break;
    case MODE_ROTAT:
    case MODE_ROTSZ:
        jtf = new double[(dims+1)*dims];
        vbuf = new double[dims*maxCount];
        if(dims == 3) buf = new double[4*maxCount];
        else buf = NULL;
        break;
    }
}

bool Internal::Initialize(void** buffer, const double* referenceCoordinates, const unsigned int count)
{
    double* matrix;
    switch(mode)
    {
    case MODE_AFFIN:
    case MODE_PROCR:
    case MODE_ORTHO:
    case MODE_UNIMO:
        matrix = new double[((dims+1)*(dims+1)+dims+1)/2 + dims];
        if(affineFitOrigin(matrix+dims, matrix, referenceCoordinates, dims, count))
        {
            *buffer = (void*) matrix;
        }
        else
        {
            delete[] matrix;
            return false;
        }
        return true;
    case MODE_ROTAT:
    case MODE_ROTSZ:
        matrix = new double[dims*count+dims];
        quaternionGetDirections(matrix+dims, matrix, referenceCoordinates, dims, count);
        *buffer = matrix;
        return true;
    }
    return true;
}

void Internal::DeInitialize(void* buffer)
{
    if(buffer != NULL) delete[] (double*)buffer;
}

void Internal::FindTransform(const double* referenceCoordinates, const double* currentCoordinates, const void* buffer, const unsigned int count)
{
    unsigned int mode = this->mode;
    if(buffer == NULL)
    {
        if(mode == MODE_AFFIN || mode == MODE_PROCR) mode = MODE_ROTSZ;
        else if(mode == MODE_ORTHO) mode = MODE_ROTAT;
    }
    switch(mode)
    {
    case MODE_AFFIN:
        affineFitCurrent(jtf, vbuf, buf, ((double*) buffer) + dims, referenceCoordinates, currentCoordinates, dims, count);
        break;
    case MODE_PROCR:
        affineFitCurrent(jtf, vbuf, buf, ((double*) buffer) + dims, referenceCoordinates, currentCoordinates, dims, count);
        affineToProcrustes(jtf, jtf, (double*) buffer, vbuf, dims);
        break;
    case MODE_ORTHO:
        affineFitCurrent(jtf, vbuf, buf, ((double*) buffer) + dims, referenceCoordinates, currentCoordinates, dims, count);
        affineToOrthogonal(jtf, jtf, (double*) buffer, vbuf, dims);
        break;
    case MODE_UNIMO:
        affineFitCurrent(jtf, vbuf, buf, ((double*) buffer) + dims, referenceCoordinates, currentCoordinates, dims, count);
        affineToUnimodular(jtf, jtf, (double*) buffer, vbuf, dims);
        break;

    case MODE_ROTAT:
        if(dims == 2)
        {
            double mean[2];
            double aver[2];
            quaternionGetDirections(vbuf, mean, currentCoordinates, 2, count);
            complexFitOrthogonal(jtf, aver, ((double*) buffer) + 2, vbuf, count);
            affineRecomputeTranslation(jtf, (double*) buffer, mean, 2);
        }
        else if(dims == 3)
        {
            double mean[3];
            double aver[4];
            quaternionGetDirections(vbuf, mean, currentCoordinates, 3, count);
            quaternionFitOrthogonal(jtf, aver, buf, ((double*) buffer) + 3, vbuf, count);
            affineRecomputeTranslation(jtf, (double*) buffer, mean, 3);
        }
        break;
    case MODE_ROTSZ:
        if(dims == 2)
        {
            double vmean[2];
            double wmean[2];
            double aver[2];
            quaternionGetDirections(vbuf, vmean, currentCoordinates, 2, count);
            complexFitOrthogonal(jtf, aver, ((double*) buffer) + dims, vbuf, count);
            affineRecomputeSizeTranslation(jtf, wmean, vmean, referenceCoordinates, currentCoordinates, 2, count);
        }
        else if(dims == 3)
        {
            double vmean[3];
            double wmean[3];
            double aver[4];
            quaternionGetDirections(vbuf, vmean, currentCoordinates, 3, count);
            quaternionFitOrthogonal(jtf, aver, buf, ((double*) buffer) + dims, vbuf, count);
            affineRecomputeSizeTranslation(jtf, wmean, vmean, referenceCoordinates, currentCoordinates, 3, count);
        }
        break;
    }
}



void Internal::GetShift(double* shift, const double* referenceCoordinates, const double* currentCoordinates)
{
    for(unsigned int k = 0; k < dims; k++)
    {
        double v = 0.0;
        for(unsigned int d = 0; d < dims; d++)
        {
            v += referenceCoordinates[d] * jtf[k*(dims+1) + d];
        }
        v += jtf[k*(dims+1) + dims];
        shift[k] = v - currentCoordinates[k];
    }
}
