#ifndef INFLUENCE_H
#define INFLUENCE_H

#include "itkImage.h"
#include "itkImageFileReader.h"
//#include "itkBlockMatchingImageFilter.h"
//#include "itkImageFileWriter.h"
//#include "itkPoint.h"
//#include "itkPointSet.h"

#include "../qmazda/SharedImage/ITKIOFactoryRegistration/itkImageIOFactoryRegisterManager.h"
#include "../qmazda/SharedImage/ITKIOFactoryRegistration/itkTransformIOFactoryRegisterManager.h"

// Refs:
//    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3985035/
//    https://itk.org/Doxygen/html/classitk_1_1fem_1_1PhysicsBasedNonRigidRegistrationMethod.html
//    https://itk.org/Doxygen/html/classitk_1_1BlockMatchingImageFilter.html
//    https://itk.org/Wiki/ITK/Examples/WishList/Registration/BlockMatchingImageFilter
//    https://github.com/InsightSoftwareConsortium/ITKWikiExamples/blob/master/WishList/Registration/BlockMatchingImageFilter.cxx
//    https://itk.org/ITKSoftwareGuide/html/Book2/ITKSoftwareGuide-Book2ch3.html
//    https://itk.org/Wiki/ITK/Examples/Registration/MutualInformation
//    https://itk.org/Wiki/ITK/Examples/Registration/ImageRegistrationMethod
//    https://itk.org/Wiki/ITK/Examples#Image_Registration
//    https://itk.org/Wiki/ITK/Examples/Registration/ImageRegistrationMethodBSpline


enum ImageMode {MODE_MAXMAD, MODE_GRADMAD, MODE_MAXHBM, MODE_GRADHBM};


template< typename TPixel, unsigned int VImageDimension >
class External
{
    typedef itk::Image<TPixel, VImageDimension> ImageType;
public:
    External(const ImageMode configMode)
    {
        mode = configMode;

        movedSearchShifts = NULL;
        movedShifts = NULL;
        fixedShifts = NULL;
        movedShiftVectors = NULL;

        blockShiftsCount = 0;
        searchShiftsCount = 0;
        blockRadius = 0;
        searchRadius = 0;
    }

    ~External()
    {
        if(fixedShifts != NULL) delete[] fixedShifts;
        if(movedShifts != NULL) delete[] movedShifts;
        if(movedSearchShifts != NULL) delete[] movedSearchShifts;
        if(movedShiftVectors != NULL) delete[] movedShiftVectors;
    }

    static const unsigned int dims = VImageDimension;

    bool loadFixed(const char *fileName)
    {
        return load(fileName, true);
    }
    bool loadMoved(const char *fileName)
    {
        return load(fileName, false);
    }

    void setRadi(double block, double search)
    {
        blockRadius = block;
        searchRadius = search;
        makeAllShifts();
    }

    TPixel* getFixedInfo(unsigned int* size, double* spacing)
    {
        unsigned int d;
        if(fixed.IsNull())
            return NULL;
        for(d = 0; d < dims; d++)
        {
            size[d] = fixed->GetLargestPossibleRegion().GetSize()[d];
            spacing[d] = fixed->GetSpacing()[d];
        }
        return fixed->GetPixelContainer()->GetImportPointer();
    }
    TPixel* getMovedInfo(unsigned int* size, double* spacing)
    {
        unsigned int d;
        if(moved.IsNull())
            return NULL;
        for(d = 0; d < dims; d++)
        {
            size[d] = moved->GetLargestPossibleRegion().GetSize()[d];
            spacing[d] = moved->GetSpacing()[d];
        }
        return moved->GetPixelContainer()->GetImportPointer();
    }



    double Mad(TPixel* ptrfx, TPixel* ptrms)
    {
        double mad = 0.0;
        for(unsigned int block = 0; block < blockShiftsCount; block++)
        {
            TPixel* ptrmb = ptrms + movedShifts[block];
            TPixel* ptrfb = ptrfx + fixedShifts[block];
            mad += fabs((double)*ptrfb - (double)*ptrmb);
        }
        return mad / blockShiftsCount;

    }


    double Hbm(TPixel* ptrfx, TPixel* ptrms)
    {
        double fixedSum = 0.0;
        double fixedSumOfSquares = 0.0;
        double movingSum = 0.0;
        double movingSumOfSquares = 0.0;
        double covariance = 0.0;

        for(unsigned int block = 0; block < blockShiftsCount; block++)
        {
            TPixel* ptrmb = ptrms + movedShifts[block];
            TPixel* ptrfb = ptrfx + fixedShifts[block];
            double fixedValue = *ptrfb;
            double movingValue = *ptrmb;
            movingSum += movingValue;
            fixedSum += fixedValue;
            movingSumOfSquares += movingValue * movingValue;
            fixedSumOfSquares += fixedValue * fixedValue;
            covariance += fixedValue * movingValue;
        }
        double fixedMean = fixedSum / blockShiftsCount;
        double movingMean = movingSum / blockShiftsCount;
        double fixedVariance = fixedSumOfSquares - blockShiftsCount * fixedMean * fixedMean;
        double movingVariance = movingSumOfSquares - blockShiftsCount * movingMean * movingMean;
        covariance -= blockShiftsCount * fixedMean * movingMean;
        if( fixedVariance * movingVariance > 0.0)
        {
            return ( covariance * covariance ) / ( fixedVariance * movingVariance );
        }
        return 0.0;
    }

    void MaxHbm(double* movedVectors, double* spacing, TPixel* ptrfx, TPixel* ptrmx)
    {
        int64_t best = 0;
        double similarity = 0.0;
        for(unsigned int search = 0; search < searchShiftsCount; search++)
        {
            TPixel* ptrms = ptrmx+movedSearchShifts[search];
            double sim = Hbm(ptrfx, ptrms);

            if( sim >= similarity )
            {
                best = search;
                similarity = sim;
            }
        }
        //double strength = similarity - sim;
        for(unsigned int d = 0; d < dims; d++)
        {
            movedVectors[d] = movedShiftVectors[best*dims+d] * spacing[d];// * strength;
        }
    }

    void MaxMad(double* movedVectors, double* spacing, TPixel* ptrfx, TPixel* ptrmx)
    {
        unsigned int  best = 0;
        double bestMad = 10000000;
        for(unsigned int search = 0; search < searchShiftsCount; search++)
        {
            TPixel* ptrms = ptrmx+movedSearchShifts[search];
            double mad = Mad(ptrfx, ptrms);
            if( bestMad >= mad )
            {
                best = search;
                bestMad = mad;
            }
        }
        for(unsigned int d = 0; d < dims; d++)
        {
            movedVectors[d] = movedShiftVectors[best*dims+d] * spacing[d];
        }
    }

    void GradientHbm(double* movedVectors, double* spacing, TPixel* ptrfx, TPixel* ptrmx)
    {
        for(unsigned int search = 0; search < searchShiftsCount; search++)
        {
            TPixel* ptrms = ptrmx+movedSearchShifts[search];
            double sim = Hbm(ptrfx, ptrms);
            ptrms = ptrmx-movedSearchShifts[search];
            sim -= Hbm(ptrfx, ptrms);
            movedVectors[search] = sim/spacing[search];
        }
    }

    void GradientMad(double* movedVectors, double* spacing, TPixel* ptrfx, TPixel* ptrmx)
    {
        for(unsigned int search = 0; search < searchShiftsCount; search++)
        {
            TPixel* ptrms = ptrmx-movedSearchShifts[search];
            double sim = Mad(ptrfx, ptrms);
            ptrms = ptrmx+movedSearchShifts[search];
            sim -= Mad(ptrfx, ptrms);
            movedVectors[search] = sim/spacing[search];
        }
    }

    void findTransform(const double* fixedVectors, double* movedVectors, const unsigned int count)
    {
        unsigned int d, n;
        if(fixed.IsNull() || moved.IsNull())
            return;
        TPixel* ptrf = fixed->GetPixelContainer()->GetImportPointer();
        TPixel* ptrm = moved->GetPixelContainer()->GetImportPointer();
        double spacing[dims];
        unsigned int sizeFixed[dims];
        unsigned int sizeMoved[dims];
        for(d = 0; d < dims; d++)
        {
            sizeFixed[d] = fixed->GetLargestPossibleRegion().GetSize()[d];
            sizeMoved[d] = moved->GetLargestPossibleRegion().GetSize()[d];
            spacing[d] = fixed->GetSpacing()[d];
        }
        int radius = blockRadius+searchRadius;

        for(n = 0; n < count; n++)
        {
            int fixedrel[dims];
            int movedrel[dims];

            for(d = 0; d < dims; d++)
            {
                fixedrel[d] = round(fixedVectors[n*dims+d] / spacing[d]);
                movedrel[d] = round(movedVectors[n*dims+d] / spacing[d]);
                if(fixedrel[d]-blockRadius < 0 || fixedrel[d]+blockRadius >= sizeFixed[d])
                    break;
                if(movedrel[d]-radius < 0 || movedrel[d]+radius >= (int)sizeFixed[d])
                    break;
            }
            if(d < dims)
            {
                for(d = 0; d < dims; d++)
                {
                    movedVectors[n*dims+d] = 0.0;
                }
                continue;
            }
            int64_t foff = fixedrel[dims-1];
            int64_t moff = movedrel[dims-1];
            for(d = dims-2; d < dims; d--)
            {
                moff *= sizeMoved[d];
                moff += movedrel[d];
                foff *= sizeFixed[d];
                foff += fixedrel[d];
            }
            TPixel* ptrfx = ptrf+foff;
            TPixel* ptrmx = ptrm+moff;

            switch(mode)
            {
            case MODE_MAXHBM: MaxHbm(movedVectors+n*dims, spacing, ptrfx, ptrmx); break;
            case MODE_MAXMAD: MaxMad(movedVectors+n*dims, spacing, ptrfx, ptrmx); break;
            case MODE_GRADHBM: GradientHbm(movedVectors+n*dims, spacing, ptrfx, ptrmx); break;
            case MODE_GRADMAD: GradientMad(movedVectors+n*dims, spacing, ptrfx, ptrmx); break;
            }
            //printf("%f %f %f : %f\n", (float)movedVectors[n*dims+0], (float)movedVectors[n*dims+1], (float)movedVectors[n*dims+2], similarity);
        }
        //printf("---------------------\n");
    }





/*
    void findTransform(const double* fixedVectors, double* movedVectors, const unsigned int count)
    {
        unsigned int d, n;
        if(fixed.IsNull() || moved.IsNull())
            return;
        unsigned char* ptrf = fixed->GetPixelContainer()->GetImportPointer();
        unsigned char* ptrm = moved->GetPixelContainer()->GetImportPointer();
        double spacing[dims];
        unsigned int sizeFixed[dims];
        unsigned int sizeMoved[dims];
        for(d = 0; d < dims; d++)
        {
            sizeFixed[d] = fixed->GetLargestPossibleRegion().GetSize()[d];
            sizeMoved[d] = moved->GetLargestPossibleRegion().GetSize()[d];
            spacing[d] = fixed->GetSpacing()[d];
        }
        int radius = blockRadius+searchRadius;

        for(n = 0; n < count; n++)
        {
            int fixedrel[dims];
            int movedrel[dims];
            int64_t best = 0;
            for(d = 0; d < dims; d++)
            {
                fixedrel[d] = round(fixedVectors[n*dims+d] / spacing[d]);
                movedrel[d] = round(movedVectors[n*dims+d] / spacing[d]);
                if(fixedrel[d]-blockRadius < 0 || fixedrel[d]+blockRadius >= sizeFixed[d])
                    break;
                if(movedrel[d]-radius < 0 || movedrel[d]+radius >= sizeFixed[d])
                    break;
            }
            if(d < dims)
            {
                for(d = 0; d < dims; d++)
                {
                    movedVectors[n*dims+d] = 0.0;
                }
                continue;
            }
            int64_t foff = fixedrel[dims-1];
            int64_t moff = movedrel[dims-1];
            for(d = dims-2; d < dims; d--)
            {
                moff *= sizeMoved[d];
                moff += movedrel[d];
                foff *= sizeFixed[d];
                foff += fixedrel[d];
            }

            unsigned char* ptrfx = ptrf+foff;
            unsigned char* ptrmx = ptrm+moff;

            double sim;
            double similarity = 0.0;
            for(unsigned int search = 0; search < searchShiftsCount; search++)
            {
                unsigned char* ptrms = ptrmx+movedSearchShifts[search];
                double fixedSum = 0.0;
                double fixedSumOfSquares = 0.0;
                double movingSum = 0.0;
                double movingSumOfSquares = 0.0;
                double covariance = 0.0;

                for(unsigned int block = 0; block < blockShiftsCount; block++)
                {
                    unsigned char* ptrmb = ptrms + movedShifts[block];
                    unsigned char* ptrfb = ptrfx + fixedShifts[block];
                    double fixedValue = *ptrfb;
                    double movingValue = *ptrmb;
                    movingSum += movingValue;
                    fixedSum += fixedValue;
                    movingSumOfSquares += movingValue * movingValue;
                    fixedSumOfSquares += fixedValue * fixedValue;
                    covariance += fixedValue * movingValue;
                }
                double fixedMean = fixedSum / blockShiftsCount;
                double movingMean = movingSum / blockShiftsCount;
                double fixedVariance = fixedSumOfSquares - blockShiftsCount * fixedMean * fixedMean;
                double movingVariance = movingSumOfSquares - blockShiftsCount * movingMean * movingMean;
                covariance -= blockShiftsCount * fixedMean * movingMean;
                sim = 0.0;
                if( fixedVariance * movingVariance )
                {
                    sim = ( covariance * covariance ) / ( fixedVariance * movingVariance );
                }
                if( sim >= similarity )
                {
                    best = search;
                    similarity = sim;
                }
            }
            //double strength = similarity - sim;
            for(d = 0; d < dims; d++)
            {
                movedVectors[n*dims+d] = movedShiftVectors[best*dims+d] * spacing[d];// * strength;
            }
            //printf("%f %f %f : %f\n", (float)movedVectors[n*dims+0], (float)movedVectors[n*dims+1], (float)movedVectors[n*dims+2], similarity);
        }
        //printf("---------------------\n");
    }

*/
/*
    void findTransform(const double* fixedVectors, double* movedVectors, const unsigned int count)
    {
        unsigned int d, n;
        if(fixed.IsNull() || moved.IsNull())
            return;
        unsigned char* ptrf = fixed->GetPixelContainer()->GetImportPointer();
        unsigned char* ptrm = moved->GetPixelContainer()->GetImportPointer();

        unsigned int numberOfVoxelInBlock = blockRadius*2+1;
        for(d = 1; d < dims; d++)
            numberOfVoxelInBlock *= (blockRadius*2+1);

        int fixedrel[dims];
        double sizeFixed[dims];
        double sizeMoved[dims];
        for(d = 0; d < dims; d++)
        {
            sizeFixed[d] = fixed->GetLargestPossibleRegion().GetSize()[d];
            sizeMoved[d] = moved->GetLargestPossibleRegion().GetSize()[d];
        }
        int radius = blockRadius+searchRadius;
        for(n = 0; n < count; n++)
        {
            for(d = 0; d < dims; d++)
            {
                fixedrel[d] = round(fixedVectors[n*dims+d]);
                if(fixedrel[d]-(int)blockRadius < 0 || fixedrel[d]+(int)blockRadius >= sizeFixed[d])
                    break;
            }
            if(d < dims)
            {
                for(d = 0; d < dims; d++)
                {
                    movedVectors[n*dims+d] = 0.0;
                }
                continue;
            }
            int minsearch[dims];
            int maxsearch[dims];
            int bestx[dims];

            for(d = 0; d < dims; d++)
            {
                bestx[d] = round(movedVectors[n*dims+d]);
                //fixedrel[d] -= bestx[d];
                minsearch[d] = bestx[d]-radius;
                maxsearch[d] = bestx[d]+radius;
                if(minsearch[d] < 0) minsearch[d] = 0;
                if(maxsearch[d] < 0) maxsearch[d] = 0;
                if(minsearch[d] >= sizeFixed[d]) minsearch[d] = sizeFixed[d];
                if(maxsearch[d] >= sizeFixed[d]) maxsearch[d] = sizeFixed[d];
                minsearch[d] += blockRadius;
                maxsearch[d] -= blockRadius;
                if(minsearch[d] > maxsearch[d])
                    break;
            }
            if(d < dims)
            {
                for(d = 0; d < dims; d++)
                {
                    movedVectors[n*dims+d] = 0.0;
                }
                continue;
            }

            double centersim = 0.0;
            double similarity = 0.0;
            int x[dims];
            for(d = 0; d < dims; d++)
                x[d] = minsearch[d];
            do
            {
                double fixedSum = 0.0;
                double fixedSumOfSquares = 0.0;
                double movingSum = 0.0;
                double movingSumOfSquares = 0.0;
                double covariance = 0.0;

                unsigned int dd, ddd;
                int xx[dims];
                for(dd = 0; dd < dims; dd++)
                    xx[dd] = -blockRadius;
                do
                {
                    unsigned int moff = xx[dims-1]+x[dims-1];
                    unsigned int foff = xx[dims-1]+fixedrel[dims-1];
                    for(ddd = dims-2; ddd < dims; ddd--)
                    {
                        moff *= sizeMoved[ddd];
                        moff += (xx[ddd]+x[ddd]);
                        foff *= sizeFixed[ddd];
                        foff += (xx[ddd]+fixedrel[ddd]);
                    }
                    double fixedValue = ptrf[foff];
                    double movingValue = ptrm[moff];
                    movingSum += movingValue;
                    fixedSum += fixedValue;
                    movingSumOfSquares += movingValue * movingValue;
                    fixedSumOfSquares += fixedValue * fixedValue;
                    covariance += fixedValue * movingValue;

                    for(dd = 0; dd < dims; dd++)
                    {
                        xx[dd]++;
                        if(xx[dd] <= (int)blockRadius)
                        {
                            break;
                        }
                        else
                        {
                            xx[dd] = -blockRadius;
                        }
                    }
                }
                while(dd < dims);

                double fixedMean = fixedSum / numberOfVoxelInBlock;
                double movingMean = movingSum / numberOfVoxelInBlock;
                double fixedVariance = fixedSumOfSquares - numberOfVoxelInBlock * fixedMean * fixedMean;
                double movingVariance = movingSumOfSquares - numberOfVoxelInBlock * movingMean * movingMean;
                covariance -= numberOfVoxelInBlock * fixedMean * movingMean;

                double sim = 0.0;
                if ( fixedVariance * movingVariance )
                {
                    sim = ( covariance * covariance ) / ( fixedVariance * movingVariance );
                }


                for(d = 0; d < dims; d++)
                {
                    if(x[d] != round(movedVectors[n*dims+d]))
                        break;
                }
                if(d >= dims)
                    centersim = sim;


                if ( sim >= similarity )
                {
                    for(dd = 0; dd < dims; dd++)
                        bestx[dd] = x[dd];
                    similarity = sim;
                }

                for(d = 0; d < dims; d++)
                {
                    x[d]++;
                    if(x[d] <= maxsearch[d])
                    {
                        break;
                    }
                    else
                    {
                        x[d] = minsearch[d];
                    }
                }
            }
            while(d < dims);

            double strength = similarity - centersim;
            for(d = 0; d < dims; d++)
            {
                movedVectors[n*dims+d] = (bestx[d] - round(movedVectors[n*dims+d]));// * strength;
            }


            printf("%f %f %f : %f\n", (float)movedVectors[n*dims+0], (float)movedVectors[n*dims+1], (float)movedVectors[n*dims+2], similarity);
        }


        printf("---------------------\n");


    }
*/


/*
    void findTransform(double* vectors, double* sims, const unsigned int count)
    {
        unsigned int n, d;
        typedef typename itk::BlockMatchingImageFilter<ImageType> BlockMatchingImageFilterType;
        typedef typename BlockMatchingImageFilterType::FeaturePointsType   PointSetType;
        typedef typename PointSetType::PointType PointType;
        typedef typename PointSetType::PointsContainerPointer PointsContainerPointer;

        if(fixed.IsNull() || moved.IsNull())
            return;

        typename PointSetType::Pointer   pointSet = PointSetType::New();
        PointsContainerPointer  points = pointSet->GetPoints();
        typename BlockMatchingImageFilterType::Pointer blockMatchingImageFilter =
                BlockMatchingImageFilterType::New();

        for(n = 0; n < count; n++)
        {
            PointType pt;
            for(d = 0; d < dims; d++)
                pt[d] = vectors[dims*n+d];
            points->InsertElement(n, pt);
        }

        typename ImageType::SizeType radius;
        radius.Fill(blockRadius);
        blockMatchingImageFilter->SetBlockRadius(radius);
        radius.Fill(searchRadius);
        blockMatchingImageFilter->SetSearchRadius(radius);

        blockMatchingImageFilter->SetFixedImage(fixed);
        blockMatchingImageFilter->SetMovingImage(moved);
        blockMatchingImageFilter->SetFeaturePoints(pointSet);
        //blockMatchingImageFilter->UpdateLargestPossibleRegion();
        blockMatchingImageFilter->Update();

        typename BlockMatchingImageFilterType::DisplacementsType * displacements =
                blockMatchingImageFilter->GetDisplacements();
        typename BlockMatchingImageFilterType::SimilaritiesType * similarities =
                blockMatchingImageFilter->GetSimilarities();

        for(n = 0; n < count; n++)
        {
            double strength = similarities->GetPointData()->ElementAt(n) - sims[n];
            sims[n] = similarities->GetPointData()->ElementAt(n);
            for(d = 0; d < dims; d++)
                vectors[dims*n+d] = displacements->GetPointData()->ElementAt(n)[d] * strength;
        }
    }
*/

private:
    ImageMode mode;

    unsigned int blockRadius;
    unsigned int searchRadius;
    typename ImageType::Pointer fixed;
    typename ImageType::Pointer moved;

    unsigned int blockShiftsCount;
    unsigned int searchShiftsCount;

    int64_t* movedSearchShifts;
    int64_t* movedShifts;
    int64_t* fixedShifts;
    int* movedShiftVectors;

    void makeOffsets(int64_t* offsets, int* vectors, unsigned int* size, int radius)
    {
        unsigned int d, dd;
        int64_t* s = offsets;
        int* v = vectors;
        int x[dims];
        for(d = 0; d < dims; d++)
            x[d] = -radius;
        do
        {
            for(d = 0; d < dims; d++)
            {
                if(x[d] != 0)
                    break;
            }
            if(d < dims)
            {
                int64_t off = x[dims-1];
                for(dd = dims-2; dd < dims; dd--)
                {
                    off *= size[dd];
                    off += x[dd];
                }
                *s = off;
                s++;

                if(v != NULL)
                {
                    for(dd = 0; dd < dims; dd++)
                    {
                        *v = x[dd];
                        v++;
                    }
                }
            }
            for(d = 0; d < dims; d++)
            {
                x[d]++;
                if(x[d] <= radius)
                {
                    break;
                }
                else
                {
                    x[d] = -radius;
                }
            }
        }
        while(d < dims);
        *s = 0;
        if(v != NULL)
        {
            for(dd = 0; dd < dims; dd++)
            {
                *v = 0;
                v++;
            }
        }
    }


    void makeOffsetsGradient(int64_t* offsets, unsigned int* size, int radius)
    {
        unsigned int d, dd;
        int64_t* s = offsets;
        for(d = 0; d < dims; d++)
        {
            int64_t off = radius;
            for(dd = d-1; dd < d; dd--)
            {
                off *= size[dd];
            }
            *s = off;
            s++;
        }
    }


    bool load(const char *fileName, bool loadFixed)
    {
        typedef itk::ImageFileReader<ImageType> ReaderType;
        try
        {
            typename ReaderType::Pointer reader = ReaderType::New();
            reader->SetFileName(fileName);
            reader->Update();
            if(loadFixed)
            {
                fixed = reader->GetOutput();
            }
            else
                moved = reader->GetOutput();
        }
        catch(...)
        {
            return false;
        }
        makeAllShifts();
        return true;
    }


    void makeAllShifts(void)
    {
        unsigned int d;
        unsigned int fsize[dims];
        unsigned int msize[dims];

        if(fixed.IsNull() || moved.IsNull())
            return;
        if(blockRadius <= 0 || searchRadius <= 0)
            return;

        blockShiftsCount = 1;
        searchShiftsCount = 1;
        for(d = 0; d < dims; d++)
        {
            fsize[d] = fixed->GetLargestPossibleRegion().GetSize()[d];
            msize[d] = moved->GetLargestPossibleRegion().GetSize()[d];
            blockShiftsCount *= (2*blockRadius+1);
            searchShiftsCount *= (2*searchRadius+1);
        }

        if(mode == MODE_GRADMAD || mode == MODE_GRADHBM)
        {
            searchShiftsCount = dims;
        }

        if(fixedShifts != NULL) delete[] fixedShifts;
        fixedShifts = new int64_t[blockShiftsCount];
        makeOffsets(fixedShifts, NULL, fsize, blockRadius);

        if(movedShifts != NULL) delete[] movedShifts;
        movedShifts = new int64_t[blockShiftsCount];
        makeOffsets(movedShifts, NULL, msize, blockRadius);

        if(movedSearchShifts != NULL) delete[] movedSearchShifts;
        movedSearchShifts = new int64_t[searchShiftsCount];
        if(movedShiftVectors != NULL) delete[] movedShiftVectors;
        if(mode == MODE_GRADMAD || mode == MODE_GRADHBM)
        {
            movedShiftVectors = NULL;
            makeOffsetsGradient(movedSearchShifts, msize, searchRadius);
        }
        else
        {
            movedShiftVectors = new int[searchShiftsCount*dims];
            makeOffsets(movedSearchShifts, movedShiftVectors, msize, searchRadius);
        }
    }

};
#endif // INFLUENCE_H
